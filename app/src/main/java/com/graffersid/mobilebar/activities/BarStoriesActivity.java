package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.BarStoriesAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.BottleDetail;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BarStoriesActivity extends BaseActivity {

    SearchView searchView;
    RelativeLayout toolLayout, toolLayout22;
    FrameLayout contentLayout;
    ImageView imgToolLayout;
    ImageButton btnFilterBottleList;
    RecyclerView recyclerView;
    List<BottleDetail> listBottle;
    Context context;
    TextView titleTxt;
    BarStoriesAdapter adapter;
    private EditText searchEditText;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private DisplayMetrics displayMetrics;
    private int height;
    private ImageButton btn_back_bottle_list;
    private BottleDetail bottleDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_stories);

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        context = BarStoriesActivity.this;

        displayMetrics = new DisplayMetrics();

        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);


        height = Math.round(displayMetrics.heightPixels);

        Universal_Var_Cls.loader(context);

        titleTxt = (TextView) findViewById(R.id.title_txt);
        btn_back_bottle_list = findViewById(R.id.btn_back_bottle_list);

        searchView = (SearchView) findViewById(R.id.search_vw_bottle_list);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_bottle_list);

        /*searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        searchEditText.setTextColor(getResources().getColor(R.color.heading_liqour));
        searchEditText.setHintTextColor(getResources().getColor(R.color.inactive_filter));
        searchEditText.setHint("Search...");*/


        toolLayout = (RelativeLayout) findViewById(R.id.tool_layout);
        toolLayout22 = (RelativeLayout) findViewById(R.id.tool_layout22);
        contentLayout = (FrameLayout) findViewById(R.id.frame_layout_bottle_list);
        imgToolLayout = (ImageView) findViewById(R.id.img_tool_layout);

        int stHeight = Universal_Var_Cls.getStatusBarHeight(context) + 20;

        toolLayout.setPadding(0, stHeight, 0, 0);

        ViewGroup.LayoutParams params = toolLayout22.getLayoutParams();
        params.height = height * 40 / 100;
        toolLayout22.setLayoutParams(params);

        RelativeLayout.LayoutParams params22 = (RelativeLayout.LayoutParams) contentLayout.getLayoutParams();
        params22.setMargins(0, params.height - 120, 0, 0);

        contentLayout.setLayoutParams(params22);

        btn_back_bottle_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        listBottle = new ArrayList<>();

        //     adapter = new BarStoriesAdapter(context, listBottle);

        prepareBottleList();

        /*searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });*/
    }

    private void prepareBottleList() {

        Universal_Var_Cls.dialog.show();

        JSONObject jsonObject = new JSONObject();

        try {
            if (Universal_Var_Cls.barId != -1)
                jsonObject.put("bar_id", Universal_Var_Cls.barId);
        } catch (JSONException e) {
        }

        final RequestQueue queue = Volley.newRequestQueue(context);

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.bottlesAtBar, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_barStories_s", response.toString());

                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);

                /*listBottle.addAll(myBarModelCls.getBottleDetail());

    //            for (int i=0; i<)

    //            listBottle = myBarModelCls.getBottleDetail();

                adapter.notifyDataSetChanged();*/
                adapter = new BarStoriesAdapter(context, myBarModelCls.getBottleDetail());

                if (myBarModelCls.getName() != null) {
                    titleTxt.setText(myBarModelCls.getName());
                }

                if (myBarModelCls.getBottleDetail() != null) {

                    String img = myBarModelCls.getImage();
                    if (img != null) {
                        Picasso.with(context).load(Universal_Var_Cls.baseUrl + myBarModelCls.getImage()).placeholder(R.drawable.consume_bottle).error(R.drawable.consume_bottle).into(imgToolLayout);
                    } else {
                        imgToolLayout.setImageResource(R.drawable.consume_bottle);
                    }
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    recyclerView.setAdapter(adapter);
                }

                Universal_Var_Cls.dialog.dismiss();

                searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        adapter.getFilter().filter(s);
                        return false;
                    }
                });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("resp_barStories_e", error.toString());
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers;
                headers = new HashMap<>();
                headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
