package com.graffersid.mobilebar.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.BarsForBottleAdapter;
import com.graffersid.mobilebar.adapter.MyBarByBottleExtValGiftAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.bars.BarsForBottle;
import com.graffersid.mobilebar.model_cls.my_bar.Datum;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BarListInByBottleActivity extends BaseActivity {

    private Context context;
    private DisplayMetrics displayMetrics;
    private int height, width;
    private TextView titleTxt;
    private SearchView searchView;
    private RecyclerView recyclerViewBar;
    private RelativeLayout toolLayout, toolLayout22;
    private FrameLayout contentLayout;
    private ImageView imgToolLayout, imgBottle;
    private LinearLayout layoutBottleDetail, btnLayStatus, layShrink, layExpanded, left_ml_layout;
    private boolean isBtnLayStatus;
    private TextView left_ml_shrink, brand_name, price_bottle, validity_bottle, left_ml_txt;
    private Button btn_renew_shrink, btn_renew_exp, btn_extend_exp, btn_gift_exp, btn_consume_history;
    private View left_ml_view;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    HashMap<String, String> headers;
    Dialog dialog;
    private WindowManager.LayoutParams lWindowParams;
    private ImageButton btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_list_in_by_bottle);

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        context = BarListInByBottleActivity.this;

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_bottle_gift_by_bottle);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        displayMetrics = new DisplayMetrics();

        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);


        height = Math.round(displayMetrics.heightPixels);
        width = Math.round(displayMetrics.widthPixels);
        width = width - (width * 10 / 100);

        int hhhhh = height - (height * 10 / 100);

        lWindowParams = new WindowManager.LayoutParams();

//        lWindowParams.copyFrom(dialog.getWindow().getAttributes());
        lWindowParams.width = width;/*WindowManager.LayoutParams.FILL_PARENT;*/ // this is where the magic happens
        lWindowParams.height = hhhhh;/*WindowManager.LayoutParams.WRAP_CONTENT;*/
        dialog.setCancelable(false);

        Universal_Var_Cls.loader(context);

        titleTxt = (TextView) findViewById(R.id.title_txt);

        searchView = (SearchView) findViewById(R.id.search_vw_bottle_list);
        recyclerViewBar = (RecyclerView) findViewById(R.id.recycler_view_bottle_list);

        btnBack = (ImageButton) findViewById(R.id.btn_back_bottle_list);

        toolLayout = (RelativeLayout) findViewById(R.id.tool_layout);
        toolLayout22 = (RelativeLayout) findViewById(R.id.tool_layout22);
        contentLayout = (FrameLayout) findViewById(R.id.frame_layout_bottle_list);
        imgToolLayout = (ImageView) findViewById(R.id.img_tool_layout);
        layoutBottleDetail = (LinearLayout) findViewById(R.id.layout_bottle_detail);
        btnLayStatus = (LinearLayout) findViewById(R.id.btn_lay_status);
        layShrink = (LinearLayout) findViewById(R.id.layout_shrink);
        layExpanded = (LinearLayout) findViewById(R.id.layout_expanded);
        left_ml_layout = (LinearLayout) findViewById(R.id.left_ml_layout);
        left_ml_view = (View) findViewById(R.id.left_ml_view);

        imgBottle = (ImageView) findViewById(R.id.bottle_img);

        left_ml_shrink = (TextView) findViewById(R.id.left_ml_shrink);
        brand_name = (TextView) findViewById(R.id.brand_name);
        price_bottle = (TextView) findViewById(R.id.price_bottle);
        validity_bottle = (TextView) findViewById(R.id.validity_bottle);
        left_ml_txt = (TextView) findViewById(R.id.left_ml_txt);

        btn_renew_shrink = (Button) findViewById(R.id.btn_renew_shrink);
        btn_renew_exp = (Button) findViewById(R.id.btn_renew_exp);
        btn_extend_exp = (Button) findViewById(R.id.btn_extend_exp);
        btn_gift_exp = (Button) findViewById(R.id.btn_gift_exp);
        btn_consume_history = (Button) findViewById(R.id.btn_consume_history);


        isBtnLayStatus = true;

        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }

        int stHeight = result + 20;

        toolLayout.setPadding(0, stHeight,0, 0);
        layoutBottleDetail.setPadding(0, stHeight,0, 0);

        final ViewGroup.LayoutParams params = toolLayout22.getLayoutParams();
        params.height = height*40/100;
        toolLayout22.setLayoutParams(params);

        final RelativeLayout.LayoutParams params22 = (RelativeLayout.LayoutParams) contentLayout.getLayoutParams();
        params22.setMargins(0, params.height-120, 0, 0);

        contentLayout.setLayoutParams(params22);


        layShrink.setVisibility(View.VISIBLE);
        layExpanded.setVisibility(View.GONE);

        btnLayStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isBtnLayStatus){
                    isBtnLayStatus = !isBtnLayStatus;
                    params.height = height*70/100;
                    toolLayout22.setLayoutParams(params);
                    params22.setMargins(0, params.height-120, 0, 0);
                    layShrink.setVisibility(View.GONE);
                    layExpanded.setVisibility(View.VISIBLE);
                }else {
                    isBtnLayStatus = !isBtnLayStatus;
                    params.height = height*40/100;
                    toolLayout22.setLayoutParams(params);
                    params22.setMargins(0, params.height-120, 0, 0);
                    layShrink.setVisibility(View.VISIBLE);
                    layExpanded.setVisibility(View.GONE);
                }
            }
        });
        btnClick();
        bottleDetailBarList();


    }

    String name = null, img = null, cat = null;
    Double pppp = 0.0;
    int qqq = 0;

    private void btnClick() {

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_renew_shrink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Universal_Var_Cls.nameBottleRenew = name;
                Universal_Var_Cls.imgBottleRenew = img;
                Universal_Var_Cls.brandNameBottleRenew = cat;
                Universal_Var_Cls.priceBottleRenew = pppp;
                Universal_Var_Cls.qtyLiqourBottleRenew = qqq;

                Universal_Var_Cls.isRenew = true;

                context.startActivity(new Intent(context, CartActivity.class));
            }
        });
        btn_renew_exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_renew_shrink.callOnClick();
            }
        });
        btn_extend_exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*editor.putInt(Universal_Var_Cls.bottlePurchasedId, Universal_Var_Cls.);
                editor.commit();
                context.startActivity(new Intent(context, ExtendValidityActivity.class));*/
                extendValidityMethod();
            }
        });
        btn_gift_exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                giftBottleMethod();
            }
        });
        btn_consume_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Universal_Var_Cls.isBottle = true;
                listBottleMethod();
  //              startActivity(new Intent(BarListInByBottleActivity.this, HistoryActivity.class));
            }
        });
    }

    private void bottleDetailBarList() {

        Universal_Var_Cls.dialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Universal_Var_Cls.barsForBottle+Universal_Var_Cls.bottleId, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();

                BarsForBottle barsForBottle = new Gson().fromJson(response.toString(), BarsForBottle.class);

                titleTxt.setText(barsForBottle.getData().get(0).getName());
                name = barsForBottle.getData().get(0).getName();

                if (barsForBottle.getData().get(0).getImage() != null){
                    Picasso.with(context).load(Universal_Var_Cls.baseUrl+barsForBottle.getData().get(0).getImage()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(imgBottle);
                    img = barsForBottle.getData().get(0).getImage();
                }
                left_ml_shrink.setText(""+barsForBottle.getData().get(0).getRemainingMl()+"ml Left");
                left_ml_txt.setText(""+barsForBottle.getData().get(0).getRemainingMl()+"ml");
                brand_name.setText(barsForBottle.getData().get(0).getCategory());
                price_bottle.setText("$ "+barsForBottle.getData().get(0).getPrice());
                pppp = barsForBottle.getData().get(0).getPrice();
                cat = barsForBottle.getData().get(0).getCategory();
                qqq = barsForBottle.getData().get(0).getTotalMl();

                if (barsForBottle.getData().get(0).getExpiry() != null){
                    try {
                        validity_bottle.setText("Validity Till "+new SimpleDateFormat("dd MMMM, yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(barsForBottle.getData().get(0).getExpiry())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                left_ml_layout.setWeightSum(barsForBottle.getData().get(0).getTotalMl());
                left_ml_view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, barsForBottle.getData().get(0).getRemainingMl()));

                Universal_Var_Cls.customerBottleId = barsForBottle.getData().get(0).getId();
                Universal_Var_Cls.bottleId = barsForBottle.getData().get(0).getBottleId();

                final BarsForBottleAdapter adapter = new BarsForBottleAdapter(context, barsForBottle.getData().get(0).getBar());

                recyclerViewBar.setHasFixedSize(true);
                recyclerViewBar.setLayoutManager(new LinearLayoutManager(context));
                recyclerViewBar.setAdapter(adapter);

                searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        adapter.getFilter().filter(s);
                        return false;
                    }
                });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }




    private void extendValidityMethod() {

        Universal_Var_Cls.isExtGift = false;

        TextView txtHeading = (TextView) dialog.findViewById(R.id.heading_dialog);
        ImageButton btnCloseDialog = (ImageButton) dialog.findViewById(R.id.close_dialog);
        final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view_gift_bottle);
        Button btnExtValidity = (Button) dialog.findViewById(R.id.gift_bottle_from_dialog);

        txtHeading.setText("Choose Bottle To Extend Validity");

        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.remove(Universal_Var_Cls.bottlePurchasedId);
                editor.commit();
                dialog.dismiss();
            }
        });

        btnExtValidity.setText("Extend Validity");

        JSONObject jsonObject = new JSONObject();
        final List<Datum> listAvailableBottle = new ArrayList<>();
        final List<Boolean> listSelected = new ArrayList<>();
        final MyBarByBottleExtValGiftAdapter adapterExt = new MyBarByBottleExtValGiftAdapter(context, listAvailableBottle, listSelected);

        try {
            jsonObject.put("bottle_id", Universal_Var_Cls.customerBottleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("json_ext_val", jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.availablePurchasedBottle, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);

                if (myBarModelCls.getData().size() == 1) {
                    editor.putInt(Universal_Var_Cls.bottlePurchasedId, myBarModelCls.getData().get(0).getId());
                    editor.commit();
                    Universal_Var_Cls.bottlePrice = myBarModelCls.getData().get(0).getPrice();
                    context.startActivity(new Intent(context, ExtendValidityActivity.class));
                } else {

                    dialog.show();
                    dialog.getWindow().setAttributes(lWindowParams);

                    listAvailableBottle.addAll(myBarModelCls.getData());

                    for (int i = 0; i < listAvailableBottle.size(); i++) {
                        listSelected.add(false);
                    }
                    adapterExt.notifyDataSetChanged();

                    Log.d("resp_ext_val", response.toString());

                    recyclerView.setHasFixedSize(false);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    recyclerView.setAdapter(adapterExt);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("resp_ext_val", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);

        btnExtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0) != 0) {
                    dialog.dismiss();
                    context.startActivity(new Intent(context, ExtendValidityActivity.class));
                }
            }
        });
    }

    private void giftBottleMethod() {

        Universal_Var_Cls.isExtGift = false;

        TextView txtHeading = (TextView) dialog.findViewById(R.id.heading_dialog);
        ImageButton btnCloseDialog = (ImageButton) dialog.findViewById(R.id.close_dialog);
        final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view_gift_bottle);
        Button btnGiftBottle = (Button) dialog.findViewById(R.id.gift_bottle_from_dialog);


        txtHeading.setText("Choose Bottle To Gift");
        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.remove(Universal_Var_Cls.bottlePurchasedId);
                editor.commit();
                dialog.dismiss();
            }
        });

        btnGiftBottle.setText("Gift");

        JSONObject jsonObject = new JSONObject();
        final List<Datum> listAvailableBottle = new ArrayList<>();
        final List<Boolean> listSelected = new ArrayList<>();
        final MyBarByBottleExtValGiftAdapter adapterExt = new MyBarByBottleExtValGiftAdapter(context, listAvailableBottle, listSelected);

        try {
            jsonObject.put("bottle_id", Universal_Var_Cls.customerBottleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.availablePurchasedBottle, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);


                if (myBarModelCls.getData().size() == 1) {
                    editor.putInt(Universal_Var_Cls.bottlePurchasedId, myBarModelCls.getData().get(0).getId());
                    editor.commit();
                    context.startActivity(new Intent(context, GiftActivity.class));
                } else {

                    dialog.show();
                    dialog.getWindow().setAttributes(lWindowParams);

                    listAvailableBottle.addAll(myBarModelCls.getData());
                    for (int i = 0; i < listAvailableBottle.size(); i++) {
                        listSelected.add(false);
                    }
                    adapterExt.notifyDataSetChanged();

                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapterExt);

                    int firstPos = layoutManager.findFirstVisibleItemPosition();
                    int lastPos = layoutManager.findLastVisibleItemPosition();

                    Log.d("visible_items", "first: " + firstPos + "   Last: " + lastPos);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);

        btnGiftBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0) != 0) {
                    dialog.dismiss();
                    context.startActivity(new Intent(context, GiftActivity.class));
                }
            }
        });
    }


    private void listBottleMethod() {

        Universal_Var_Cls.isExtGift = false;

        TextView txtHeading = (TextView) dialog.findViewById(R.id.heading_dialog);
        ImageButton btnCloseDialog = (ImageButton) dialog.findViewById(R.id.close_dialog);
        final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view_gift_bottle);
        Button btnGiftBottle = (Button) dialog.findViewById(R.id.gift_bottle_from_dialog);


        txtHeading.setText("History");
        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.remove(Universal_Var_Cls.bottlePurchasedId);
                editor.commit();
                dialog.dismiss();
            }
        });

        btnGiftBottle.setVisibility(View.VISIBLE);
        btnGiftBottle.setText("View History");

        JSONObject jsonObject = new JSONObject();
        final List<Datum> listAvailableBottle = new ArrayList<>();
        final List<Boolean> listSelected = new ArrayList<>();
        final MyBarByBottleExtValGiftAdapter adapterExt = new MyBarByBottleExtValGiftAdapter(context, listAvailableBottle, listSelected);

        try {
            jsonObject.put("bottle_id", Universal_Var_Cls.customerBottleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.availablePurchasedBottle, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);

                if (myBarModelCls.getData().size() == 1) {

                    Universal_Var_Cls.bottleIdPurchased = myBarModelCls.getData().get(0).getId();
                    editor.putInt(Universal_Var_Cls.bottlePurchasedId, myBarModelCls.getData().get(0).getId());
                    editor.commit();
                    context.startActivity(new Intent(context, HistoryActivity.class));
                } else {

                    dialog.show();
                    dialog.getWindow().setAttributes(lWindowParams);

                    listAvailableBottle.addAll(myBarModelCls.getData());
                    for (int i = 0; i < listAvailableBottle.size(); i++) {
                        listSelected.add(false);
                    }
                    adapterExt.notifyDataSetChanged();

                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapterExt);

                    int firstPos = layoutManager.findFirstVisibleItemPosition();
                    int lastPos = layoutManager.findLastVisibleItemPosition();

                    Log.d("visible_items", "first: " + firstPos + "   Last: " + lastPos);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);


        btnGiftBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0) != 0) {
                    dialog.dismiss();
                    context.startActivity(new Intent(context, HistoryActivity.class));
                }
            }
        });
    }
}
