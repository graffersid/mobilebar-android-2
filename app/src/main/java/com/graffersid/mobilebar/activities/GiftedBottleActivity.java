package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;

import android.os.Bundle;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.GiftedBottleAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GiftedBottleActivity extends BaseActivity {

    ImageButton btnBack;
    RecyclerView recyclerView;
    GiftedBottleAdapter adapter;
    public static List<Result> list;
    Context context;

    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    private static String nextPage, previousPage;
    private Map<String, String> headers;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    Drawable wrappedDrawable;
    LinearLayout layoutMlLeft, layoutDetail;
    TextView txtMlLeft, titleTxt;
    View viewMlLeft;
    TextView datePurchased, nameBottle, nameCat, mailId, dateGifted, dateExp;
    ImageView imgBottle;
    private TextView noHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gifted_bottle);

        context = GiftedBottleActivity.this;

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(context, R.drawable.back_left_ml_bar_in_bottle);
        wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);

        Universal_Var_Cls.loader(context);


        btnBack = (ImageButton) findViewById(R.id.back_btn_gifted_bottle);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_vw_gifted_bottle);

        layoutMlLeft = (LinearLayout) findViewById(R.id.left_ml_layout);
        txtMlLeft = (TextView) findViewById(R.id.left_ml_txt);
        viewMlLeft = (View) findViewById(R.id.left_ml_view);

        titleTxt = (TextView) findViewById(R.id.toolbar_title_gift);
        layoutDetail = (LinearLayout) findViewById(R.id.gifted_bottle_detail_layout);

        noHistory = (TextView) findViewById(R.id.no_history);

        noHistory.setVisibility(View.GONE);

        imgBottle = (ImageView) findViewById(R.id.img_bottle_history);

        datePurchased = (TextView) findViewById(R.id.purchased_date);
        nameBottle = (TextView) findViewById(R.id.bottle_name);
        nameCat = (TextView) findViewById(R.id.brand_name);
        mailId = (TextView) findViewById(R.id.mail_id);
        dateGifted = (TextView) findViewById(R.id.date_gifted);
        dateExp = (TextView) findViewById(R.id.exp_date);

        DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.inactive_filter));
        layoutMlLeft.setBackgroundResource(R.drawable.back_left_ml_bar_in_bottle);

        DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.bar_line_color));
        viewMlLeft.setBackgroundResource(R.drawable.back_left_ml_bar_in_bottle);

        layoutDetail.setVisibility(View.GONE);

        list = new ArrayList<>();
        layoutManager = new GridLayoutManager(context, 1);
        adapter = new GiftedBottleAdapter(context, list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);


        layoutMlLeft.setWeightSum(100);


        Universal_Var_Cls.dialog.show();
        giftedList(Universal_Var_Cls.gifted_list);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isPaginate = true;
                                giftedList(nextPage);
                            }
                        }, 100);
                    }
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (layoutDetail.getVisibility() == View.VISIBLE){
            layoutDetail.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            titleTxt.setText("GIFTS");
            Universal_Var_Cls.mailId = null;
        }else {
            super.onBackPressed();
        }
    }

    public void giftedList(String url) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (Universal_Var_Cls.dialog.isShowing()){
                    Universal_Var_Cls.dialog.dismiss();
                }

                MyBarModelCls modelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);


                if (modelCls.getNext() != null){

                    String str = modelCls.getNext();

                    if (str.contains("localhost")){
                        str = str.replace("localhost", Universal_Var_Cls.baseDomain);
                    }
                    nextPage = str;
                }else {
                    nextPage = null;
                }

                if (modelCls.getResults() != null && !modelCls.getResults().isEmpty()) {
                    list.addAll(modelCls.getResults());
                    adapter.notifyDataSetChanged();
                    noHistory.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }else {
                    noHistory.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (Universal_Var_Cls.dialog.isShowing()){
                    Universal_Var_Cls.dialog.dismiss();
                }
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    public void detailBottle(int id) {

        titleTxt.setText("GIFT DETAIL");
        layoutDetail.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        noHistory.setVisibility(View.GONE);

        Universal_Var_Cls.dialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Universal_Var_Cls.giftedDetail+id, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();

                MyBarModelCls modelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);

                nameBottle.setText(modelCls.getData().get(0).getBottleDetail());
                nameCat.setText(modelCls.getData().get(0).getCategoryId());

                if (modelCls.getData().get(0).getImage() != null){
                    Picasso.with(context).load(Universal_Var_Cls.baseUrl+modelCls.getData().get(0).getImage()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(imgBottle);
                }else {
                    imgBottle.setImageResource(R.drawable.bottle_gray);
                }

                if (Universal_Var_Cls.mailId != null) {
                    mailId.setText(Universal_Var_Cls.mailId);
                }

                try {
                    datePurchased.setText(new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(modelCls.getData().get(0).getPurchasedDate())));
                    dateGifted.setText(new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(modelCls.getData().get(0).getGiftDate())));
                    dateExp.setText(new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(modelCls.getData().get(0).getExpiry())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                int total = modelCls.getData().get(0).getBottleMl();
                int gifted = modelCls.getData().get(0).getGiftedMl();
                int percentLeft = gifted*100/total;

                txtMlLeft.setText(""+percentLeft+"%");

                viewMlLeft.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, percentLeft));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
