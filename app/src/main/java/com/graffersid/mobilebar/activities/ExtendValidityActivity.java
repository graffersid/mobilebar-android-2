package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.ExtendValidityAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.Datum;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExtendValidityActivity extends BaseActivity {

    ImageButton btnBack;
    private List<Datum> list;
    private ExtendValidityAdapter adapter;
    private LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extend_validity);

        context = ExtendValidityActivity.this;
        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        btnBack = (ImageButton) findViewById(R.id.back_btn_ext_val);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_vw_ext);
        list = new ArrayList<>();


        getRedeemPointsAndTax();

 //       extendValidityListMethod();

        recyclerViewOnScroll();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0) != 0) {
            editor.remove(Universal_Var_Cls.bottlePurchasedId);
            editor.commit();
        }
        super.onBackPressed();
    }

    private void getRedeemPointsAndTax() {

        Universal_Var_Cls.loader(ExtendValidityActivity.this);
        Universal_Var_Cls.dialog.show();

        String url = Universal_Var_Cls.customerReedemPointAndTax;

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("response_list_redeem", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.getJSONArray("data").get(0).toString());

                    Universal_Var_Cls.redeemPoint = jsonObject.getInt("point");
                    Universal_Var_Cls.taxDefault = jsonObject.getDouble("tax_value");
                    Universal_Var_Cls.redeemPointFactor = jsonObject.getDouble("redeem_factor");

                    /*Universal_Var_Cls.taxDefault = response.getJSONObject("data").getDouble("default_tax");
                    Universal_Var_Cls.redeemPointFactor = response.getJSONObject("data").getDouble("point_redeem_factor");
                    Universal_Var_Cls.redeemPoint = response.getJSONObject("data").getInt("point_redeem_value");
                    Universal_Var_Cls.publishableKey = response.getJSONObject("data").getString("payment_publishable_key");
                    Universal_Var_Cls.secretKey = response.getJSONObject("data").getString("admin_payment_key");*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                extendValidityListMethod();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("response_list_redeem", error.toString());
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }
    private void extendValidityListMethod() {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Universal_Var_Cls.extendValidityList, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();

                list = new Gson().fromJson(response.toString(), MyBarModelCls.class).getData();

                Comparator<Datum> compareById = new Comparator<Datum>() {
                    @Override
                    public int compare(Datum o1, Datum o2) {
                        return o1.getDays().compareTo(o2.getDays());
                    }
                };

                Collections.sort(list, compareById);

                Log.d("resp_ext_list", response.toString()+"  "+list.get(0).getDays());

                layoutManager = new LinearLayoutManager(ExtendValidityActivity.this, LinearLayoutManager.HORIZONTAL, false);

                adapter = new ExtendValidityAdapter(ExtendValidityActivity.this, list, true);
 //               layoutManager = new LinearLayoutManager(ExtendValidityActivity.this);

                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapter);

                adapter.notifyDataSetChanged();

               /* editor.remove(Universal_Var_Cls.bottlePurchasedId);
                editor.commit();*/

                /*int ff = layoutManager.findFirstVisibleItemPosition();
                int ll = layoutManager.findLastVisibleItemPosition();

                Log.d("resp_ext_list", ff+"  "+ll);*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("resp_ext_list", error.toString());
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        });
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private void recyclerViewOnScroll() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int ff = layoutManager.findFirstVisibleItemPosition();
                int ll = layoutManager.findLastVisibleItemPosition();

                Log.d("resp_ext_list", ff+"  "+ll);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int firstPos = layoutManager.findFirstVisibleItemPosition();
                int lastPos = layoutManager.findLastVisibleItemPosition();
                int middle = Math.abs(lastPos - firstPos) / 2 + firstPos;

                int selectedPos = -1;

                for (int i = 0; i < adapter.getItemCount(); i++) {
                    if (i == middle) {
         //               adapter.getItem(i).setSelected(true);
                        selectedPos = i;
                    } else {
        //                adapter.getItem(i).setSelected(false);
                    }
                }

                adapter.notifyDataSetChanged();
            }
        });
    }
}
