package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.MySharedPref;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MobileOtpVerifyActivity extends BaseActivity {

    ImageButton btnBack;
    TextView title;
    LinearLayout layoutMobile, layoutPin, layoutEnterOtp;
    EditText edtTxtCountryCode, edtTxtEnterMobile, edtOtp, edtOtp2, edtOtp3, edtOtp4, edtPin, edtPin2, edtPin3, edtPin4;
    Button btnVerifyMobile, btnResendOtp, btnSubmitOtp, btnCreatePin;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    HashMap<String, String> headers;
    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_otp_verify);

        context = MobileOtpVerifyActivity.this;

        Universal_Var_Cls.loader(context);

        initialize();
    }

    private void initialize() {

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();


        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        title = (TextView) findViewById(R.id.toolbar_title_mobile_verify);
        btnBack = (ImageButton) findViewById(R.id.back_btn_mobile_verify);
        layoutMobile = (LinearLayout) findViewById(R.id.layout_mobile);
//        layoutPin = (LinearLayout) findViewById(R.id.layout_pin);


        edtTxtEnterMobile = (EditText) findViewById(R.id.edt_txt_enter_mobile);
        edtTxtCountryCode = (EditText) findViewById(R.id.edt_txt_country_code);
        btnVerifyMobile = (Button) findViewById(R.id.btn_verify_mobile);

        layoutEnterOtp = (LinearLayout) findViewById(R.id.layout_enter_otp);
        edtOtp = (EditText) findViewById(R.id.otp);
        edtOtp2 = (EditText) findViewById(R.id.otp2);
        edtOtp3 = (EditText) findViewById(R.id.otp3);
        edtOtp4 = (EditText) findViewById(R.id.otp4);
        btnResendOtp = (Button) findViewById(R.id.btn_resend_otp);
        btnSubmitOtp = (Button) findViewById(R.id.btn_submit_otp);

  /*      edtPin = (EditText) findViewById(R.id.pin);
        edtPin2 = (EditText) findViewById(R.id.pin2);
        edtPin3 = (EditText) findViewById(R.id.pin3);
        edtPin4 = (EditText) findViewById(R.id.pin4);*/
//        btnCreatePin = (Button) findViewById(R.id.btn_create_pin);


        layoutMobile.setVisibility(View.VISIBLE);
        layoutEnterOtp.setVisibility(View.GONE);
 //       layoutPin.setVisibility(View.GONE);

        title.setText("MOBILE VERIFICATION");

        edtTxtChngEvent();
        btnClick();
    }

    private void edtTxtChngEvent() {

        edtOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtOtp.getText().toString().length() == 1) {
                    edtOtp2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtOtp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtOtp2.getText().toString().length() == 1) {
                    edtOtp3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtOtp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtOtp3.getText().toString().length() == 1) {
                    edtOtp4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*edtPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtPin.getText().toString().length() == 1) {
                    edtPin2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtPin2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtPin2.getText().toString().length() == 1) {
                    edtPin3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtPin3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtPin3.getText().toString().length() == 1) {
                    edtPin4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/
    }


    private void btnClick() {

        btnVerifyMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edtTxtCountryCode.getText().toString().length() > 1) {

                    String str = edtTxtCountryCode.getText().toString();

                    char x = str.charAt(0);
                    if (x == '+'){

                        if (edtTxtEnterMobile.getText().toString().length() > 0)
                            verifyMobile();
                        else edtTxtEnterMobile.setError("Enter Mobile Number than click on verify button");
                    }else {
                        edtTxtCountryCode.setError("Enter valid country code");
                    }
                }else edtTxtCountryCode.setError("Enter country code");
            }
        });
        btnSubmitOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtOtp.getText().toString().length() > 0 && edtOtp2.getText().toString().length() > 0 && edtOtp3.getText().toString().length() > 0 && edtOtp4.getText().toString().length() > 0) {

                    String otp = edtOtp.getText().toString().trim();
                    String otp2 = edtOtp2.getText().toString().trim();
                    String otp3 = edtOtp3.getText().toString().trim();
                    String otp4 = edtOtp4.getText().toString().trim();
                    String otpStr = otp + otp2 + otp3 + otp4;
                    verifyOtp(otpStr);
                } else validOtp();
            }
        });
        /*btnCreatePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtPin.getText().toString().length() > 0 && edtPin2.getText().toString().length() > 0 && edtPin3.getText().toString().length() > 0 && edtPin4.getText().toString().length() > 0) {

                    String otp = edtPin.getText().toString().trim();
                    String otp2 = edtPin2.getText().toString().trim();
                    String otp3 = edtPin3.getText().toString().trim();
                    String otp4 = edtPin4.getText().toString().trim();
                    String otpStr = otp + otp2 + otp3 + otp4;
                    createPin(otpStr);
                } else {
                    Toast.makeText(MobileOtpVerifyActivity.this, "Enter pin correct", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Universal_Var_Cls.pinCustomer = 0;

        if (Universal_Var_Cls.isMobileVerification){
            Intent intent = new Intent();
            intent.putExtra("mobile", edtTxtCountryCode.getText().toString().trim()+edtTxtEnterMobile.getText().toString().trim());
            setResult(RESULT_OK, intent);
        }
        super.onBackPressed();
    }

    private void validOtp() {

        Toast.makeText(context, "Enter correct OTP", Toast.LENGTH_SHORT).show();
    }

    private void verifyMobile() {

        Universal_Var_Cls.dialog.show();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("phone", edtTxtEnterMobile.getText().toString().trim());
            jsonObject.put("country_code", edtTxtCountryCode.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.verifyCustomerNo, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_verify_mob", response.toString());

                Toast.makeText(MobileOtpVerifyActivity.this, "OTP has been sent", Toast.LENGTH_SHORT).show();

                Universal_Var_Cls.dialog.dismiss();
                edtTxtEnterMobile.setEnabled(false);
                edtTxtCountryCode.setEnabled(false);

                try {
                    if (response.getBoolean("status")) {
                        btnVerifyMobile.setVisibility(View.GONE);
                        layoutEnterOtp.setVisibility(View.VISIBLE);
                    } else {

                        Toast.makeText(MobileOtpVerifyActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();

                VolleyErrorCls.volleyErr(error, context);
                Log.d("resp_verify_mob", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void verifyOtp(String otpStr) {

        Universal_Var_Cls.dialog.show();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("phone", edtTxtEnterMobile.getText().toString().trim());
            jsonObject.put("otp", otpStr);
            jsonObject.put("country_code", edtTxtCountryCode.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.checkUserOTP, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_verify_otp", response.toString());

                Universal_Var_Cls.dialog.dismiss();
                Toast.makeText(MobileOtpVerifyActivity.this, "OTP verified successfully", Toast.LENGTH_SHORT).show();

                try {
                    if (response.getBoolean("status")) {

                        /*if (Universal_Var_Cls.pinCustomer != 0){
                            Intent intent = new Intent(MobileOtpVerifyActivity.this, ConsumeLiquorActivity.class);
                            *//*intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(Intent.CATEGORY_HOME);*//*
                            startActivity(intent);
     //                       finish();
                        }else {
             *//*               layoutMobile.setVisibility(View.GONE);
                            layoutPin.setVisibility(View.VISIBLE);*//*

                            Intent intent = new Intent(MobileOtpVerifyActivity.this, ConsumeLiquorActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            startActivity(intent);
                            finish();
                        }*/

                        System.out.println("--- phone verified successfully -------");
                        MySharedPref.saveData(MobileOtpVerifyActivity.this,"isVerified",true);

                        if (Universal_Var_Cls.isMobileVerification){
                            onBackPressed();
                        }else {
                            Intent intent = new Intent(MobileOtpVerifyActivity.this, ConsumeLiquorActivity.class);
                            startActivity(intent);
                        }
                    } else {

                        Toast.makeText(MobileOtpVerifyActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
                Log.d("resp_verify_otp", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }



    private void createPin(String otpStr) {

        Universal_Var_Cls.dialog.show();

        JSONObject jsonObject = new JSONObject();

        try {
   //         jsonObject.put("phone", edtTxtEnterMobile.getText().toString().trim());
            jsonObject.put("pin", otpStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.generatePin, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();

                Log.d("resp_verify_pin", response.toString());

                Toast.makeText(MobileOtpVerifyActivity.this, "New PIN has been generated", Toast.LENGTH_SHORT).show();

                try {
                    if (response.getBoolean("status")) {

                        Universal_Var_Cls.pinCustomer = Integer.parseInt(response.getString("data"));
 //                       Universal_Var_Cls.pinCustomer = 1234;

                        Intent intent = new Intent(MobileOtpVerifyActivity.this, ConsumeLiquorActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        startActivity(intent);
                        finish();
                    } else {

                        Toast.makeText(MobileOtpVerifyActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
                Log.d("resp_verify_pin", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
