package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.ConnectivityReceiver;
import com.graffersid.mobilebar.classes.FabricCrashlytics;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BaseActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    Handler mHandler;
    Runnable runnable;
    boolean isStarted=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);
        mHandler = new Handler();
        startProgress();

        getAdminData();
    }


    private void startProgress() {
        mHandler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                checkInternetConnection();
                mHandler.postDelayed(this, 2000);
            }
        };
        mHandler.postDelayed(runnable, 1000);
    }

    private void checkInternetConnection() {
        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo;
        networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            isStarted=false;
//            Toast.makeText(BaseActivity.this, "Network Available", Toast.LENGTH_LONG).show();

        } else {
            if (!isStarted)
            showAlertDialog();

        }
    }

    private void showAlertDialog() {
        mHandler.removeCallbacks(runnable);

        isStarted=true;
            new AlertDialog.Builder(BaseActivity.this)
                    .setTitle("Network Disconnect")
                    .setMessage("Connection Unavailable ")

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            isStarted=false;
                            startProgress();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setCancelable(false)
                    .show();


    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        System.out.println("------- is internet Connected ------- " + isConnected);
        showSnack(isConnected);

    }

    @Override
    protected void onResume() {
        super.onResume();
        // register connection status listener
        FabricCrashlytics.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }


        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    public String trimImageUrl(String imageUrl)
    {
        String image="";
        String[] arrImg = imageUrl.split("/");
        if (arrImg[1].equalsIgnoreCase(arrImg[2]))
        {
            for (int i=2;i<arrImg.length;i++){

                image=image+"/"+arrImg[i];

            }
            return image;
        }

        return imageUrl;
    }


    private void getAdminData() {

        String url = Universal_Var_Cls.adminSetting;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_bottle_save_", response.toString());

                try {
                    Universal_Var_Cls.publishableKey = response.getString("publish_ky");

                    System.out.println("----- default image ---- ");
                    System.out.println(trimImageUrl(response.getString("def_bottle_image")));

                    Universal_Var_Cls.imgUrl = Universal_Var_Cls.baseUrl+trimImageUrl(response.getString("def_user_image"));
                    Universal_Var_Cls.barImgUrl = Universal_Var_Cls.baseUrl+trimImageUrl(response.getString("def_bar_image"));
                    Universal_Var_Cls.bottleImgUrl = Universal_Var_Cls.baseUrl+trimImageUrl(response.getString("def_bottle_image"));
                    Universal_Var_Cls.catImgUrl = Universal_Var_Cls.baseUrl+trimImageUrl(response.getString("def_categ_image"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyErrorCls.volleyErr(error, BaseActivity.this);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
              SharedPreferences preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        RequestQueue queue = Volley.newRequestQueue(BaseActivity.this);
        queue.add(request);


    }

}
