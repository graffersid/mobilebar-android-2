package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.frags.FilterBarFragment;
import com.graffersid.mobilebar.frags.FilterCategoryFragment;
import com.graffersid.mobilebar.frags.FilterPriceRangeFragment;
import com.graffersid.mobilebar.model_cls.filter_category_list.ResultCategory;
import com.graffersid.mobilebar.model_cls.liquor_list.LiquorBottleListCls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.graffersid.mobilebar.activities.LiquorActivity.filterBtn;
import static com.graffersid.mobilebar.classes.MySharedPref.getData;
import static com.graffersid.mobilebar.classes.MySharedPref.save;
import static com.graffersid.mobilebar.classes.MySharedPref.saveData;

public class FilterActivity extends BaseActivity {

    Context context;

    private Toolbar toolbar;
    public static TabLayout tabLayout;
    private ViewPager viewPager;
    private PagerAdapter adapter;
    TextView tabIndicator1, tabIndicator2, tabIndicator3;
    private ImageButton backBtn;
    private Button clearFilterBtn, applyFilterBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = FilterActivity.this;


        clearFilterBtn = (Button) findViewById(R.id.clear_filter_btn);
        applyFilterBtn = (Button) findViewById(R.id.apply_filter_btn);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.toolbar_filter_layout);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.pager);
        setupViewPager(viewPager);

        tabIndicator1 = (TextView) findViewById(R.id.tab_indicator1);
        tabIndicator2 = (TextView) findViewById(R.id.tab_indicator2);
        tabIndicator3 = (TextView) findViewById(R.id.tab_indicator3);

        backBtn = (ImageButton) toolbar.findViewById(R.id.back_btn_from_filter);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        clearFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterBtn.setText("Filter");
                Universal_Var_Cls.filterCategoryIdList.clear();
                Universal_Var_Cls.filterBarIdList.clear();
                try {
                    Universal_Var_Cls.jsonObject.put("category", new JSONArray());
                    Universal_Var_Cls.jsonObject.put("bar", new JSONArray());
                    Universal_Var_Cls.jsonObject.put("range_start", "");
                    Universal_Var_Cls.jsonObject.put("range_end", "");

                    ResultCategory resultCategory;
                    for (int i = 0; i < Universal_Var_Cls.listcategory.size(); i++) {

                        resultCategory = new ResultCategory();
                        resultCategory.setId(Universal_Var_Cls.listcategory.get(i).getId());
                        resultCategory.setImage(Universal_Var_Cls.listcategory.get(i).getImage());
                        resultCategory.setName(Universal_Var_Cls.listcategory.get(i).getName());
                        resultCategory.setIsSelect(false);
                        Universal_Var_Cls.filterCategoryIdList.add("-1");
                        Universal_Var_Cls.listcategory.set(i, resultCategory);
                        FilterCategoryFragment.adapter.notifyDataSetChanged();
                    }
                    for (int i = 0; i < Universal_Var_Cls.listBar.size(); i++) {

                        resultCategory = new ResultCategory();
                        resultCategory.setId(Universal_Var_Cls.listBar.get(i).getId());
                        resultCategory.setImage(Universal_Var_Cls.listBar.get(i).getImage());
                        resultCategory.setName(Universal_Var_Cls.listBar.get(i).getName());
                        resultCategory.setAddress(Universal_Var_Cls.listBar.get(i).getAddress());
                        resultCategory.setIsSelect(false);
                        Universal_Var_Cls.filterBarIdList.add("-1");
                        Universal_Var_Cls.listBar.set(i, resultCategory);
                        FilterBarFragment.adapter.notifyDataSetChanged();
                    }
                    Universal_Var_Cls.minSelectedFilterPrice = 0;
                    Universal_Var_Cls.maxSelectedFilterPrice = 0;
//                    FilterPriceRangeFragment.minPrice.setText(0);
//                    FilterPriceRangeFragment.maxPrice.setText(0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                saveData(FilterActivity.this,"filterCount" ,"0");
                FilterActivity.tabLayout.getTabAt(0).setText("CATEGORY");
                FilterActivity.tabLayout.getTabAt(1).setText("BAR");
            }
        });
        applyFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterBtn.setText("Filter");
                JSONArray jsonArrayCategory = new JSONArray();
                JSONArray jsonArrayBar = new JSONArray();

                for (int i = 0; i < Universal_Var_Cls.filterCategoryIdList.size(); i++) {
                    if (!(Universal_Var_Cls.filterCategoryIdList.get(i).equals("-1")))
                        jsonArrayCategory.put(Integer.parseInt(Universal_Var_Cls.filterCategoryIdList.get(i)));
                }


                for (int i = 0; i < Universal_Var_Cls.filterBarIdList.size(); i++) {
                    if (!(Universal_Var_Cls.filterBarIdList.get(i).equals("-1")))
                        jsonArrayBar.put(Integer.parseInt(Universal_Var_Cls.filterBarIdList.get(i)));
                }

                try {
                    Universal_Var_Cls.jsonObject.put("category", jsonArrayCategory);
                    Universal_Var_Cls.jsonObject.put("bar", jsonArrayBar);

                    System.out.println(" ------ min selected price --- " + Universal_Var_Cls.minSelectedFilterPrice);
                    System.out.println(" ------ max selected price --- " + Universal_Var_Cls.maxSelectedFilterPrice);

                    if (Universal_Var_Cls.minSelectedFilterPrice == 0) {
                        Universal_Var_Cls.jsonObject.put("range_start", "0");
                    } else {
                        Universal_Var_Cls.jsonObject.put("range_start", "" + Universal_Var_Cls.minSelectedFilterPrice);
                    }
                    if (Universal_Var_Cls.maxSelectedFilterPrice == 0)
                        Universal_Var_Cls.jsonObject.put("range_end", "");
                    else
                    Universal_Var_Cls.jsonObject.put("range_end", "" + Universal_Var_Cls.maxSelectedFilterPrice);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("liqour_bottle_list_j", Universal_Var_Cls.jsonObject.toString());

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.bottleListUrl,
                        Universal_Var_Cls.jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("liqour_bottle_list", response.toString());

                        Universal_Var_Cls.liqourBottleListCls = new Gson().fromJson(response.toString(), LiquorBottleListCls.class);


                       startActivity(new Intent(FilterActivity.this, LiquorActivity.class));

                        onBackPressed();


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyErrorCls.volleyErr(error, context);
                    }
                });

                RequestQueue queue = Volley.newRequestQueue(context);
                queue.add(request);
            }
        });
    }

    private void setupViewPager(final ViewPager viewPager) {

        adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FilterCategoryFragment());
        adapter.addFragment(new FilterBarFragment());
        adapter.addFragment(new FilterPriceRangeFragment());
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    tabIndicator1.setVisibility(View.VISIBLE);
                    tabIndicator2.setVisibility(View.GONE);
                    tabIndicator3.setVisibility(View.GONE);
                }

                if (tab.getPosition() == 1) {
                    tabIndicator1.setVisibility(View.GONE);
                    tabIndicator2.setVisibility(View.VISIBLE);
                    tabIndicator3.setVisibility(View.GONE);
                }

                if (tab.getPosition() == 2) {
                    tabIndicator1.setVisibility(View.GONE);
                    tabIndicator2.setVisibility(View.GONE);
                    tabIndicator3.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                Fragment fragment = ((PagerAdapter) viewPager.getAdapter()).getFragment(i);
                if (i >= 0 && fragment != null) {
                    fragment.onResume();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    class PagerAdapter extends FragmentPagerAdapter {

        private Map<Integer, String> fragmentTags;
        FragmentManager fragmentManager;

        private final List<Fragment> mFragmentList = new ArrayList<>();

        public PagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
            this.fragmentManager = supportFragmentManager;
            fragmentTags = new HashMap<>();
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {

            Object obj = super.instantiateItem(container, position);

            if (obj instanceof Fragment) {
                Fragment f = (Fragment) obj;
                String tag = f.getTag();
                fragmentTags.put(position, tag);
            }
            return obj;
        }

        public Fragment getFragment(int position) {
            String tag = fragmentTags.get(position);
            if (tag == null) {
                return null;
            }

            return fragmentManager.findFragmentByTag(tag);
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }

    private void setupTabIcons() {

        int countCat = 0;
        for (int i = 0; i<Universal_Var_Cls.filterCategoryIdList.size(); i++){

            if (!Universal_Var_Cls.filterCategoryIdList.get(i).equals("-1")) {
                countCat += 1;
            }
        }
        int countBar = 0;
        for (int i = 0; i<Universal_Var_Cls.filterBarIdList.size(); i++){

            if (!Universal_Var_Cls.filterBarIdList.get(i).equals("-1")) {
                countBar += 1;
            }
        }
        int countPrice=0;
        if (Universal_Var_Cls.maxSelectedFilterPrice>0)
            countPrice=1;

        System.out.println("----- selected Price ----- " + Universal_Var_Cls.maxSelectedFilterPrice);

        tabLayout.getTabAt(0).setText("CATEGORY("+countCat+")");
        tabLayout.getTabAt(1).setText("BAR("+countBar+")");
        tabLayout.getTabAt(2).setText("PRICE RANGE");
        //     tabLayout.setTabTextColors(getResources().getColor(R.color.blue), getResources().getColor(R.color.design_default_color_primary));

        System.out.println(countPrice);
        System.out.println(countCat);
        System.out.println(countBar);
        System.out.println("----- is count is > 0 " + (countCat+countBar+countPrice));
        if ((countCat+countBar+countPrice) > 0) {
            saveData(FilterActivity.this,"filterCount" ,String.valueOf((countCat + countBar+countPrice)));
            filterBtn.setText("Filter(" + (countCat + countBar + countPrice) + ")");
        }else {
            filterBtn.setText("Filter");
            saveData(FilterActivity.this,"filterCount" ,"0");
        }
    }
}
