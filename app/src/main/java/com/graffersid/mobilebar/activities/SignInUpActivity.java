package com.graffersid.mobilebar.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.CurrentLocation;
import com.graffersid.mobilebar.classes.FingerLoginConsume;
import com.graffersid.mobilebar.classes.FingerprintHandler;
import com.graffersid.mobilebar.classes.MySharedPref;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.login_model_cls.fb_login.FbLogin_Model_Cls;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static com.graffersid.mobilebar.classes.MySharedPref.getData;


public class SignInUpActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String KEY_NAME = "mobileBar";
    Button signUpBtn, fbLoginBtn, googleLoginBtn, fbSignUpBtn, googleSignUpBtn, signInBtn;
    CheckBox ageVerify, terms_condition_btn;
    EditText referralCode;
    LinearLayout signInLayout, signUpLayout;
    ImageButton fingerImgBtn, faceImgBtn;
    LoginButton loginButton;
    Context context;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean boolSignUp;
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private TextView textview_terms;
    private View mContentView;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private GoogleApiClient mGoogleSignInClient;
    private int RC_SIGN_IN = 0;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            System.out.println(" ------- >>>>.  login result ------ " + loginResult);
            loadUserProfile(loginResult);

        }

        @Override

        public void onCancel() {
        }

        @Override
        public void onError(FacebookException e) {
            e.printStackTrace();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_up);

        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };
        accessTokenTracker.startTracking();
        mContentView = findViewById(R.id.fullscreen_sign_in);
        mContentView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        context = SignInUpActivity.this;

        signUpBtn = (Button) findViewById(R.id.sign_up_btn);
        textview_terms = findViewById(R.id.textview_terms);

        fingerImgBtn = (ImageButton) findViewById(R.id.finger_img_btn);
        faceImgBtn = (ImageButton) findViewById(R.id.faceID_img_btn);
        fbLoginBtn = (Button) findViewById(R.id.fb_sign_in);
        googleLoginBtn = (Button) findViewById(R.id.google_sign_in);
        fbSignUpBtn = (Button) findViewById(R.id.fb_sign_up_btn);
        googleSignUpBtn = (Button) findViewById(R.id.google_sign_up_btn);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        signInBtn = (Button) findViewById(R.id.sign_in_btn);
        ageVerify = (CheckBox) findViewById(R.id.age_verify_btn);
        terms_condition_btn = (CheckBox) findViewById(R.id.terms_condition_btn);
        signInLayout = (LinearLayout) findViewById(R.id.sign_in_layout);
        signUpLayout = (LinearLayout) findViewById(R.id.sign_up_layout);
        referralCode = (EditText) findViewById(R.id.referral_code_edt_txt);

        customTextView(textview_terms);


        ageVerify.setOnCheckedChangeListener(this);
        terms_condition_btn.setOnCheckedChangeListener(this);

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        boolSignUp = false;


        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInLayout.setVisibility(View.GONE);
                signUpLayout.setVisibility(View.VISIBLE);
                Universal_Var_Cls.isSignUp = true;
            }
        });
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUpLayout.setVisibility(View.GONE);
                signInLayout.setVisibility(View.VISIBLE);
                Universal_Var_Cls.isSignUp = false;
            }
        });



        fingerImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                new FingerLoginConsume(SignInUpActivity.this).fingerPrintLogin();
//                fingerPrintLogin();


                BiometricManager biometricManager = BiometricManager.from(SignInUpActivity.this);

                if (biometricManager.canAuthenticate() == BiometricManager.BIOMETRIC_SUCCESS) {
                    BiometricPrompt.PromptInfo promptInfo = getPromptInfo();
                    prompt().authenticate(promptInfo);
                } else
                    Log.d("Face Authentication : ", "could not authenticate because: $canAuthenticate");
            }
        });


//        faceImgBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                faceIdDetection(view);
//            }
//        });


        fbLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolSignUp = false;

                LoginManager.getInstance().logOut();
                loginButton.performClick();
            }
        });
        fbSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolSignUp = true;
                loginButton.performClick();
            }
        });
        fbLoginMethod();
        googleLoginMethod();

        Universal_Var_Cls.isSignIn = true;

        if (preferences.getString(Universal_Var_Cls.loginStr, null) != null) {
            new FingerLoginConsume(SignInUpActivity.this).fingerPrintLogin();
        }

    }

    private BiometricPrompt prompt() {
        Executor executor = ContextCompat.getMainExecutor(SignInUpActivity.this);

        return new BiometricPrompt((FragmentActivity) SignInUpActivity.this, executor,
                new BiometricPrompt.AuthenticationCallback() {
                    @Override
                    public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                        super.onAuthenticationError(errorCode, errString);
                        Log.e("Face Authentication : ", errString.toString());
                    }
                    @Override
                    public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                        super.onAuthenticationSucceeded(result);
                        proceedToNext();
                    }
                    @Override
                    public void onAuthenticationFailed() {
                        super.onAuthenticationFailed();
                        Log.e("Face Authentication : ", " Failed");
                    }
                });
    }

    private void proceedToNext() {
        if (Universal_Var_Cls.isSignUp){

        }else if (Universal_Var_Cls.isSignIn && preferences.getString(Universal_Var_Cls.loginStr, null) != null){

            SignInUpActivity activity = (SignInUpActivity) context;

            JSONObject jsonObject = null;
            JSONObject jsonObject22 = new JSONObject();
            try {
                jsonObject = new JSONObject(preferences.getString(Universal_Var_Cls.loginStr, null));

                jsonObject22.put("name", jsonObject.getJSONObject("user").getString("name"));
                jsonObject22.put("image", jsonObject.getJSONObject("user").getString("image"));

                new CurrentLocation(context).lattLong();

                jsonObject22.put("xcord", Universal_Var_Cls.lat);
                jsonObject22.put("ycord", Universal_Var_Cls.lng);
                jsonObject22.put("reffer_code", "");

                if (preferences.getString(Universal_Var_Cls.loginType, null).equals("facebook")){

                    jsonObject.put("user", jsonObject22);
                    activity.signInUsingFacebookGoogle(jsonObject, Universal_Var_Cls.signInWithFacebook, preferences.getString(Universal_Var_Cls.loginType, null));
                }else if (preferences.getString(Universal_Var_Cls.loginType, null).equals("google")){

                    jsonObject22.put("person_number", "");
                    jsonObject.put("user", jsonObject22);
                    activity.signInUsingFacebookGoogle(jsonObject, Universal_Var_Cls.signInWithGoogle, preferences.getString(Universal_Var_Cls.loginType, null));
                }else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }/*else if (Universal_Var_Cls.pinCustomer != 0){

            ConsumeLiquorActivity activity = (ConsumeLiquorActivity) context;
            activity.verifyCustomerPin();
        }else {
            Toast.makeText(context, "Please enter amount of liquor", Toast.LENGTH_SHORT).show();
            new FingerLoginConsume(context).fingerPrintLogin();
        }*/
    }


    private BiometricPrompt.PromptInfo getPromptInfo() {

        return new BiometricPrompt.PromptInfo.Builder()
                .setTitle("MobileBar App's Authentication")
                .setSubtitle("Please login to get access")
                .setDescription("MobileBaris using Android biometric authentication")
                .setConfirmationRequired(true)
                .setDeviceCredentialAllowed(true)
                .build();

    }
//
//
//    private void faceIdDetection(View view) {
//
//        FaceDetector detector = new FaceDetector.Builder(context)
//                .setTrackingEnabled(false)
//                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
//                .build();
//
//
//    }

    private void customTextView(TextView view) {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                "I agree to the ");
        spanTxt.append("Term of services");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://172.104.52.17:3000/terms-conditions"));
                startActivity(browserIntent);
            }
        }, spanTxt.length() - "Term of services".length(), spanTxt.length(), 0);
        spanTxt.append(" and");
        spanTxt.setSpan(new ForegroundColorSpan(Color.BLACK), 32, spanTxt.length(), 0);
        spanTxt.append(" Privacy Policy");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://172.104.52.17:3000/privacy-policy"));
                startActivity(browserIntent);
            }
        }, spanTxt.length() - " Privacy Policy".length(), spanTxt.length(), 0);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }

    private void googleLoginMethod() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button





//        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        googleLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolSignUp = false;
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);

//                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
//                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
        googleSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolSignUp = true;
//
//                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
//                startActivityForResult(signInIntent, RC_SIGN_IN);
//

                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);

            }
        });
    }

    /*@Override
    protected void onStart() {
        super.onStart();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }*/

    private void fbLoginMethod() {

//        loginButton.setReadPermissions(Arrays.asList("EMAIL"));
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, callback);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }

    private void handleSignInResult(GoogleSignInResult completedTask) {


        try {
            Log.d("Google Plus Signin ", "handleSignInResult:" + completedTask.isSuccess());
//            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
//
//            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);

            GoogleSignInAccount acct = completedTask.getSignInAccount();
            if (completedTask.isSuccess()) {
                String personName = acct.getDisplayName();
                String personGivenName = acct.getGivenName();
                String personFamilyName = acct.getFamilyName();
                String personEmail = acct.getEmail();

                String personId = acct.getId();

                Uri personPhoto = null;
                if (acct.getPhotoUrl() != null) {
                    personPhoto = acct.getPhotoUrl();
                }

                Log.d("google_resp", "Hi" + "\npersonName : " + personName + "\npersonGivenName : " + personGivenName + "\npersonFamilyName : " + personFamilyName + "\npersonEmail : " + personEmail + "\npersonId : " + personId + "\npersonPhoto : " + personPhoto);

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("google", personId);
                    jsonObject.put("email", personEmail);
                    JSONObject jsonObject22 = new JSONObject();
                    jsonObject22.put("name", personName);
                    if (personPhoto != null) {
                        jsonObject22.put("image", personPhoto);
                    } else {
                        jsonObject22.put("image", "");
                    }
                    jsonObject22.put("phone", JSONObject.NULL);

                    new CurrentLocation(SignInUpActivity.this).lattLong();
                    jsonObject22.put("xcord", Universal_Var_Cls.lat);
                    jsonObject22.put("ycord", Universal_Var_Cls.lng);

                    if (referralCode.getText() != null) {
                        jsonObject22.put("reffer_code", referralCode.getText().toString());
                    } else {
                        jsonObject22.put("reffer_code", "");
                    }
                    jsonObject.put("user", jsonObject22);

                    Log.d("google_resp", jsonObject.toString());

                    LoginManager.getInstance().logOut();
                    Auth.GoogleSignInApi.signOut(mGoogleSignInClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
//                                    updateUI(false);
                                }
                            });

//                    mGoogleSignInClient.signOut()
//                            .addOnCompleteListener(this, new OnCompleteListener<Void>() {
//                                @Override
//                                public void onComplete(@NonNull Task<Void> task) {
//                                    // ...
//                                }
//                            });


                    if (personName != null) {
                        signInUsingFacebookGoogle(jsonObject, Universal_Var_Cls.signInWithGoogle, "google");
                    } else {
                        //             googleLoginBtn.callOnClick();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            /*mGoogleSignInClient.signOut()
                    .addOnCompleteListener(SignInUpActivity.this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // ...
                        }
                    });*/
        } catch (Exception e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
e.printStackTrace();

//            String sssts = e.getMessage() + "   " + e.stat
//            Log.w("google_resp_e", "" + sssts);
            Log.w("google_resp_e", "" + e);
            //          updateUI(null);
            Toast.makeText(context, "Some thing went wrong. Please try again", Toast.LENGTH_SHORT).show();
        }
    }


    private void loadUserProfile(LoginResult newAccessToken) {

        GraphRequest request = GraphRequest.newMeRequest(newAccessToken.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                FbLogin_Model_Cls fbLogin_model_cls = new Gson().fromJson(object.toString(), FbLogin_Model_Cls.class);

                JSONObject jsonObject = new JSONObject();
                try {
                    String firstName = object.getString("name");
                    String email = object.getString("email");
                    String id = object.getString("id");
                    //             String contact_number = object. getString("contact_number");
                    String imageUrl = "https://graph.facebook.com/" + id + "/picture?type=normal";

                    Log.d("FB RESULT", "Hi\n" + "Name: " + firstName + "\nemail: " + email + "\nid: " + id + "\nimageUrl: " + imageUrl);


                    jsonObject.put("facebook", fbLogin_model_cls.getId());
                    jsonObject.put("email", fbLogin_model_cls.getEmail());
                    JSONObject jsonObject22 = new JSONObject();
                    jsonObject22.put("name", fbLogin_model_cls.getName());

                    if (fbLogin_model_cls.getPicture() != null) {
                        jsonObject22.put("image", fbLogin_model_cls.getPicture());
                    } else {
                        jsonObject22.put("image", "");
                    }

                    new CurrentLocation(SignInUpActivity.this).lattLong();
                    jsonObject22.put("xcord", Universal_Var_Cls.lat);
                    jsonObject22.put("ycord", Universal_Var_Cls.lng);
                    if (referralCode.getText() != null) {
                        jsonObject22.put("reffer_code", referralCode.getText().toString());
                    } else {
                        jsonObject22.put("reffer_code", "");
                    }
                    jsonObject22.put("phone", JSONObject.NULL);
                    //          jsonObject22.put("pin", JSONObject.NULL);
                    jsonObject.put("user", jsonObject22);

                    LoginManager.getInstance().logOut();

                    signInUsingFacebookGoogle(jsonObject, Universal_Var_Cls.signInWithFacebook, "facebook");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //             Log.d("jkjdhkada", "Hi\n"+object.toString());
            }
        });

        Bundle parameters = new Bundle();

        parameters.putString("fields", "id, name, email, picture.width(120).height(120)");
        request.setParameters(parameters);

        request.executeAsync();
    }

    private void fingerPrintLogin() {

        // If you’ve set your app’s minSdkVersion to anything lower than 23, then you’ll need to verify that the device is running Marshmallow
        // or higher before executing any fingerprint-related code
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Get an instance of KeyguardManager and FingerprintManager//
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

            //Check whether the device has a fingerprint sensor//
            if (!fingerprintManager.isHardwareDetected()) {
                // If a fingerprint sensor isn’t available, then inform the user that they’ll be unable to use your app’s fingerprint functionality//
                //           textView.setText("Your device doesn't support fingerprint authentication");
            }
            //Check whether the user has granted your app the USE_FINGERPRINT permission//
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                // If your app doesn't have this permission, then display the following text//
                //           textView.setText("Please enable the fingerprint permission");
            }

            //Check that the user has registered at least one fingerprint//
            if (!fingerprintManager.hasEnrolledFingerprints()) {
                // If the user hasn’t configured any fingerprints, then display the following message//
                //           textView.setText("No fingerprint configured. Please register at least one fingerprint in your device's Settings");
            }

            //Check that the lockscreen is secured//
            if (!keyguardManager.isKeyguardSecure()) {
                // If the user hasn’t secured their lockscreen with a PIN password or pattern, then display the following text//
                //           textView.setText("Please enable lockscreen security in your device's Settings");
            } else {
                try {
                    generateKey();
                } catch (FingerprintException e) {
                    e.printStackTrace();
                }

                if (initCipher()) {
                    //If the cipher is initialized successfully, then create a CryptoObject instance//
                    cryptoObject = new FingerprintManager.CryptoObject(cipher);

                    // Here, I’m referencing the FingerprintHandler class that we’ll create in the next section. This class will be responsible
                    // for starting the authentication process (via the startAuth method) and processing the authentication process events//
                    FingerprintHandler helper = new FingerprintHandler(this);
                    helper.startAuth(fingerprintManager, cryptoObject);
                }
            }
        }
    }


    //Create the generateKey method that we’ll use to gain access to the Android keystore and generate the encryption key//

    @TargetApi(Build.VERSION_CODES.M)
    private void generateKey() throws FingerprintException {
        try {
            // Obtain a reference to the Keystore using the standard Android keystore container identifier (“AndroidKeystore”)//
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            //Generate the key//
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            //Initialize an empty KeyStore//
            keyStore.load(null);

            //Initialize the KeyGenerator//
            keyGenerator.init(new
                    //Specify the operation(s) this key can be used for//
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)

                    //Configure this key so that the user has to confirm their identity with a fingerprint each time they want to use it//
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            //Generate the key//
            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }
    }

    //Create a new method that we’ll use to initialize our cipher//
    @TargetApi(Build.VERSION_CODES.M)
    public boolean initCipher() {
        try {
            //Obtain a cipher instance and configure it with the properties required for fingerprint authentication//
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //Return true if the cipher has been initialized successfully//
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            //Return false if cipher initialization failed//
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        verifyCheckbox();
    }

    private void verifyCheckbox() {

        if (ageVerify.isChecked() && terms_condition_btn.isChecked()) {
            fbSignUpBtn.setBackgroundResource(R.drawable.back_fb_sign_in_btn);
            googleSignUpBtn.setBackgroundResource(R.drawable.back_google_sign_in_btn);
            fbSignUpBtn.setEnabled(true);
            googleSignUpBtn.setEnabled(true);
        } else {
            fbSignUpBtn.setBackgroundResource(R.drawable.back_google_fb_btn);
            googleSignUpBtn.setBackgroundResource(R.drawable.back_google_fb_btn);
            fbSignUpBtn.setEnabled(false);
            googleSignUpBtn.setEnabled(false);
        }
    }

    public void signInUsingFacebookGoogle(final JSONObject jsonObject, String url, final String loginType) {

        Log.d("resp_login_url", url);
        Log.d("resp_login_json", jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_login", response.toString());

                Universal_Var_Cls.isSignIn = false;
                try {
                    if (response.has("data") && response.getString("data").length() > 0) {

                        editor.putString(Universal_Var_Cls.loginType, loginType);
                        editor.putString(Universal_Var_Cls.loginStr, jsonObject.toString());
                        editor.commit();
                        getAdminData();
                        try {
                            String token = "Token " + response.getString("data");
                            editor.putString(Universal_Var_Cls.toKen, token);
                            editor.putString(Universal_Var_Cls.imgUrl, jsonObject.getJSONObject("user").getString("image"));
                            editor.commit();
                            String phone = jsonObject.getJSONObject("user").getString("phone");

                            System.out.println("-------- phone ------ " + phone);
                            if (phone == null || phone.length() == 0 || phone.equalsIgnoreCase("null"))
                                MySharedPref.saveData(SignInUpActivity.this, "isVerified", false);
                            else
                                MySharedPref.saveData(SignInUpActivity.this, "isVerified", true);

                            boolean isVerified = getData(SignInUpActivity.this, "isVerified", false);
                            System.out.println("------ is phone verfied ------ " + isVerified);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (Universal_Var_Cls.statusLogin == 2) {

                            startActivity(new Intent(SignInUpActivity.this, LiquorActivity.class));
                        } else {
                            Intent intent = new Intent(SignInUpActivity.this, MyBarActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            startActivity(intent);
                            finish();
                            //                      startActivity(new Intent(SignInUpActivity.this, MyBarActivity.class));
                        }
                    } else {
                        Toast.makeText(SignInUpActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("resp_login_e", error.toString());
                error.printStackTrace();
                new FingerLoginConsume(SignInUpActivity.this).fingerPrintLogin();

                VolleyErrorCls.volleyErr(error, context);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(SignInUpActivity.this);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void getAdminData() {

        String url = Universal_Var_Cls.adminSetting;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_bottle_save_", response.toString());

                Universal_Var_Cls.dialog.dismiss();

                try {
                    Universal_Var_Cls.publishableKey = response.getString("publish_ky");

                    System.out.println("----- default image ---- ");
                    System.out.println(trimImageUrl(response.getString("def_bottle_image")));

                    Universal_Var_Cls.imgUrl = Universal_Var_Cls.baseUrl+trimImageUrl(response.getString("def_user_image"));
                    Universal_Var_Cls.barImgUrl = Universal_Var_Cls.baseUrl+trimImageUrl(response.getString("def_bar_image"));
                    Universal_Var_Cls.bottleImgUrl = Universal_Var_Cls.baseUrl+trimImageUrl(response.getString("def_bottle_image"));
                    Universal_Var_Cls.catImgUrl = Universal_Var_Cls.baseUrl+trimImageUrl(response.getString("def_categ_image"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();

                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private class FingerprintException extends Exception {
        public FingerprintException(Exception e) {
            super(e);
        }
    }


}
