package com.graffersid.mobilebar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.LiquorAdapter;
import com.graffersid.mobilebar.classes.CartHelper;
import com.graffersid.mobilebar.classes.CurrentLocation;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.frags.FilterBarFragment;
import com.graffersid.mobilebar.frags.FilterCategoryFragment;
import com.graffersid.mobilebar.model_cls.filter_category_list.FilterCategoryList;
import com.graffersid.mobilebar.model_cls.filter_category_list.ResultCategory;
import com.graffersid.mobilebar.model_cls.liquor_list.LiquorBottleListCls;
import com.graffersid.mobilebar.model_cls.liquor_list.ResultBottle;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.graffersid.mobilebar.classes.MySharedPref.getData;
import static com.graffersid.mobilebar.classes.MySharedPref.saveData;
import static com.graffersid.mobilebar.classes.Universal_Var_Cls.listBottle;

public class LiquorActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static Button filterBtn, sortBtn;
    public static RelativeLayout counterLayout;
    public static TextView counterTxt;
    public static SearchView searchView;
    public static EditText searchEditText;
    private static String nextPage, previousPage;
    RecyclerView recyclerView;
    LiquorAdapter adapter;
    Activity context;
    SwipeRefreshLayout swipeRefreshLayout;
    GridLayoutManager layoutManager;
    ImageButton cartBtn;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    DrawerLayout drawer;
    boolean loaderIsVisible;
    boolean boolSearch = false;
    boolean boolBackSearch = false;
    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    private LinearLayout bottleNotAvailableLayout;
    private Toolbar toolbar;
    private CartHelper helper;
    private SQLiteDatabase database;
    private CircleImageView imgUser;
    private TextView nameUser;
    private Button btnEditProfile, btnMyBar, btnBottleHistory, btnTransactionHistory, btnConsumptionHistory, btnDeals, btnGifts, btnReferFriend, btnT_and_C, btnSignInOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liquor);

        System.out.println("------- onCreate -------- ");
        context = LiquorActivity.this;

        loaderIsVisible = true;

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        //     toggle.getDrawerArrowDrawable().setColor(Color.RED);
        toolbar.setNavigationIcon(R.drawable.menu_black);
        //    toggle.setHomeAsUpIndicator(R.drawable.menu_black);

        Universal_Var_Cls.loader(context);

        Universal_Var_Cls.dialog.show();
        loaderIsVisible = false;

        new CurrentLocation(this).lattLong();


        Log.d("call_back_s", "onStart   " + Universal_Var_Cls.filterCategoryIdList.isEmpty());


        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        helper = new CartHelper(context);
        database = helper.getWritableDatabase();

        recyclerView = (RecyclerView) findViewById(R.id.liqour_list_recycler_vw);
        filterBtn = (Button) findViewById(R.id.filter_btn);
        sortBtn = (Button) findViewById(R.id.sort_btn);
        bottleNotAvailableLayout = (LinearLayout) findViewById(R.id.no_bottle_available);
        cartBtn = (ImageButton) findViewById(R.id.cart_btn);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);

        counterLayout = (RelativeLayout) findViewById(R.id.counter_layout);
        counterTxt = (TextView) findViewById(R.id.counter_txt);

//        clearFIlter();
        if (Universal_Var_Cls.counter==0) {
            Cursor cursor = database.query(CartHelper.TABLE_CART, null, null,
                    null, null, null, null);

            if (cursor != null) {

                Universal_Var_Cls.counter = cursor.getCount();
                if (Universal_Var_Cls.counter == 0) {
                    counterLayout.setVisibility(View.GONE);
                }
            }
        }

        bottleNotAvailableLayout.setVisibility(View.GONE);

        imgUser = (CircleImageView) findViewById(R.id.img_user);
        nameUser = (TextView) findViewById(R.id.name_user);
        btnEditProfile = (Button) findViewById(R.id.edit_profile);
        btnMyBar = (Button) findViewById(R.id.my_bar_btn);
        btnBottleHistory = (Button) findViewById(R.id.bottle_history_btn);
        btnTransactionHistory = (Button) findViewById(R.id.transaction_history_btn);
        btnConsumptionHistory = (Button) findViewById(R.id.consumption_history_btn);
        btnDeals = (Button) findViewById(R.id.deals_btn);
        btnGifts = (Button) findViewById(R.id.gifts_btn);
        btnReferFriend = (Button) findViewById(R.id.refer_friend_btn);
        btnT_and_C = (Button) findViewById(R.id.t_and_c_btn);
        btnSignInOut = (Button) findViewById(R.id.sign_in_out_btn);


        String img = preferences.getString(Universal_Var_Cls.imgUrl, null);

        if (preferences.getString(Universal_Var_Cls.toKen, null) != null) {

            if (img != null && !img.equals("null") && img.length() > 0) {
                Picasso.with(this).load(preferences.getString(Universal_Var_Cls.imgUrl, null)).placeholder(R.drawable.default_img).error(R.drawable.default_img).into(imgUser);
            } else {
                imgUser.setBackgroundResource(R.drawable.default_img);
            }
            if (preferences.getString(Universal_Var_Cls.loginStr, null) != null) {

                try {
                    JSONObject jsonObject = new JSONObject(preferences.getString(Universal_Var_Cls.loginStr, null));
                    nameUser.setText(jsonObject.getJSONObject("user").getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            btnSignInOut.setText("Sign Out");
            btnMyBar.setVisibility(View.VISIBLE);
            btnBottleHistory.setVisibility(View.VISIBLE);
            btnTransactionHistory.setVisibility(View.VISIBLE);
            btnConsumptionHistory.setVisibility(View.VISIBLE);
            btnGifts.setVisibility(View.VISIBLE);
            btnReferFriend.setVisibility(View.VISIBLE);
            btnEditProfile.setVisibility(View.VISIBLE);
        } else {
            imgUser.setBackgroundResource(R.drawable.default_img);
            btnSignInOut.setText("Sign In");
            imgUser.setBackgroundResource(0);
            nameUser.setText("Guest");

            btnMyBar.setVisibility(View.GONE);
            btnBottleHistory.setVisibility(View.GONE);
            btnTransactionHistory.setVisibility(View.GONE);
            btnConsumptionHistory.setVisibility(View.GONE);
            btnGifts.setVisibility(View.GONE);
            btnReferFriend.setVisibility(View.GONE);
            btnEditProfile.setVisibility(View.GONE);
        }


        listBottle = new ArrayList<>();

        layoutManager = new GridLayoutManager(context, 2);
        adapter = new LiquorAdapter(context, listBottle);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        if (!Universal_Var_Cls.statusFilter) {
            Universal_Var_Cls.statusFilter = true;
            Universal_Var_Cls.jsonInitialize();
        }

        int count = Integer.parseInt(getData(LiquorActivity.this, "filterCount", "0"));

        if (Universal_Var_Cls.maxSelectedFilterPrice>0)
            count=count+1;

        System.out.println("-------- filter count ------ " + count);

        if (count> 0) {
            filterBtn.setText("Filter " + "(" + count + ")");
        }


    }

    private void clearFIlter() {

        System.out.println(" - ------- filter clear ------ ");
        Universal_Var_Cls.filterCategoryIdList.clear();
        Universal_Var_Cls.filterBarIdList.clear();
        try {
            Universal_Var_Cls.jsonObject.put("category", new JSONArray());
            Universal_Var_Cls.jsonObject.put("bar", new JSONArray());
            Universal_Var_Cls.jsonObject.put("range_start", "");
            Universal_Var_Cls.jsonObject.put("range_end", "");

            ResultCategory resultCategory;

            if (Universal_Var_Cls.listcategory != null) {

                for (int i = 0; i < Universal_Var_Cls.listcategory.size(); i++) {

                    resultCategory = new ResultCategory();
                    resultCategory.setId(Universal_Var_Cls.listcategory.get(i).getId());
                    resultCategory.setImage(Universal_Var_Cls.listcategory.get(i).getImage());
                    resultCategory.setName(Universal_Var_Cls.listcategory.get(i).getName());
                    resultCategory.setIsSelect(false);
                    Universal_Var_Cls.filterCategoryIdList.add("-1");
                    Universal_Var_Cls.listcategory.set(i, resultCategory);
                    FilterCategoryFragment.adapter.notifyDataSetChanged();
                }
            }
            if (Universal_Var_Cls.listBar != null) {
                for (int i = 0; i < Universal_Var_Cls.listBar.size(); i++) {

                    resultCategory = new ResultCategory();
                    resultCategory.setId(Universal_Var_Cls.listBar.get(i).getId());
                    resultCategory.setImage(Universal_Var_Cls.listBar.get(i).getImage());
                    resultCategory.setName(Universal_Var_Cls.listBar.get(i).getName());
                    resultCategory.setAddress(Universal_Var_Cls.listBar.get(i).getAddress());
                    resultCategory.setIsSelect(false);
                    Universal_Var_Cls.filterBarIdList.add("-1");
                    Universal_Var_Cls.listBar.set(i, resultCategory);
                    FilterBarFragment.adapter.notifyDataSetChanged();
                }
            }
            Universal_Var_Cls.minSelectedFilterPrice = 0;
            Universal_Var_Cls.maxSelectedFilterPrice = 0;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        filterBtn.setText("Filter");
    }

    @Override
    protected void onResume() {
        super.onResume();
        clickMethod();
        paginationMethod();
        liqourListData(Universal_Var_Cls.bottleListUrl);
        initDialogSortAndFilter();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void clickMethod() {


        btnT_and_C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Universal_Var_Cls.baseUrl+"/terms-conditions"));
                startActivity(browserIntent);

            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                liqourListData(Universal_Var_Cls.bottleListUrl);

            }
        });

        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (preferences.getString(Universal_Var_Cls.toKen, null) != null) {
                    startActivity(new Intent(LiquorActivity.this, CartActivity.class));
                } else {
                    Universal_Var_Cls.statusLogin = 1;
                    startActivity(new Intent(LiquorActivity.this, SignInUpActivity.class));
                }
            }
        });

        btnSignInOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (btnSignInOut.getText().toString().equals("Sign Out")) {

                    editor.remove(Universal_Var_Cls.toKen);
                    editor.remove(Universal_Var_Cls.imgUrl);
                    editor.remove(Universal_Var_Cls.loginStr);
                    editor.remove(Universal_Var_Cls.loginType);
                    editor.commit();

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }

                    btnSignInOut.setText("Sign In");
                    //                   imgUser.setImageResource(0);
                    imgUser.setImageResource(R.drawable.default_img);
                    nameUser.setText("Guest");
                    btnMyBar.setVisibility(View.GONE);
                    btnBottleHistory.setVisibility(View.GONE);
                    btnTransactionHistory.setVisibility(View.GONE);
                    btnConsumptionHistory.setVisibility(View.GONE);
                    btnGifts.setVisibility(View.GONE);
                    btnReferFriend.setVisibility(View.GONE);
                    btnEditProfile.setVisibility(View.GONE);
                    database.delete(CartHelper.TABLE_CART, null, null);
                } else {
                    Universal_Var_Cls.statusLogin = 2;
                    startActivity(new Intent(LiquorActivity.this, SignInUpActivity.class));
                }
            }
        });
        btnDeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFIlter();
                closeDrawer();
                startActivity(new Intent(LiquorActivity.this, DealsActivity.class));
            }
        });
        btnMyBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFIlter();
                saveData(LiquorActivity.this, "filterCount", "0");
                startActivity(new Intent(LiquorActivity.this, MyBarActivity.class));
            }
        });
        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFIlter();
                startActivity(new Intent(LiquorActivity.this, ProfileActivity.class));
            }
        });
        btnBottleHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                clearFIlter();
                Universal_Var_Cls.statusHistory = 1;
                startActivity(new Intent(LiquorActivity.this, HistoryActivity.class));
            }
        });
        btnTransactionHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                clearFIlter();
                Universal_Var_Cls.statusHistory = 2;
                startActivity(new Intent(LiquorActivity.this, HistoryActivity.class));
            }
        });
        btnConsumptionHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                Universal_Var_Cls.statusHistory = 3;
                startActivity(new Intent(LiquorActivity.this, HistoryActivity.class));
            }
        });
        btnGifts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                clearFIlter();
                startActivity(new Intent(LiquorActivity.this, GiftedBottleActivity.class));
            }
        });
        btnReferFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                clearFIlter();
                startActivity(new Intent(LiquorActivity.this, ReferFriendActivity.class));
            }
        });
    }

    private void closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    private void initDialogSortAndFilter() {

        Universal_Var_Cls.filterBarList = new FilterCategoryList();
        Universal_Var_Cls.filterCategoryList = new FilterCategoryList();
        priceMinMaxFilter();
        barList();
        categoryList();
        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                priceMinMaxFilter();
                barList();
                categoryList();

                startActivity(new Intent(LiquorActivity.this, FilterActivity.class));
            }
        });

        sortBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_sort_bottle_list);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();

                ImageButton closeSortDialog = (ImageButton) dialog.findViewById(R.id.close_sort_dialog);
                TextView lowToHigh = (TextView) dialog.findViewById(R.id.sort_low_to_high);
                TextView highToLow = (TextView) dialog.findViewById(R.id.sort_high_to_low);
                TextView aToZ = (TextView) dialog.findViewById(R.id.sort_a_to_z);
                TextView zToA = (TextView) dialog.findViewById(R.id.sort_z_to_a);

                closeSortDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                lowToHigh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listBottle = new ArrayList<>();
                        adapter = new LiquorAdapter(context, listBottle);
                        setRecyclerViewData();

                        try {
                            Universal_Var_Cls.jsonObject.put("sort_price_up_to_down", "111");
                            Universal_Var_Cls.jsonObject.put("sort_price_down_to_up", "");
                            Universal_Var_Cls.jsonObject.put("character_asscending", "");
                            Universal_Var_Cls.jsonObject.put("character_decending", "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        loaderIsVisible = true;
                        liqourListData(Universal_Var_Cls.bottleListUrl);
                    }
                });
                highToLow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listBottle = new ArrayList<>();
                        adapter = new LiquorAdapter(context, listBottle);
                        setRecyclerViewData();

                        try {
                            Universal_Var_Cls.jsonObject.put("sort_price_up_to_down", "");
                            Universal_Var_Cls.jsonObject.put("sort_price_down_to_up", "111");
                            Universal_Var_Cls.jsonObject.put("character_asscending", "");
                            Universal_Var_Cls.jsonObject.put("character_decending", "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        loaderIsVisible = true;
                        liqourListData(Universal_Var_Cls.bottleListUrl);
                    }
                });
                aToZ.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listBottle = new ArrayList<>();
                        adapter = new LiquorAdapter(context, listBottle);
                        setRecyclerViewData();

                        try {
                            Universal_Var_Cls.jsonObject.put("sort_price_up_to_down", "");
                            Universal_Var_Cls.jsonObject.put("sort_price_down_to_up", "");
                            Universal_Var_Cls.jsonObject.put("character_asscending", "a");
                            Universal_Var_Cls.jsonObject.put("character_decending", "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        loaderIsVisible = true;
                        liqourListData(Universal_Var_Cls.bottleListUrl);
                    }
                });
                zToA.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listBottle = new ArrayList<>();
                        adapter = new LiquorAdapter(context, listBottle);
                        setRecyclerViewData();

                        try {
                            Universal_Var_Cls.jsonObject.put("sort_price_up_to_down", "");
                            Universal_Var_Cls.jsonObject.put("sort_price_down_to_up", "");
                            Universal_Var_Cls.jsonObject.put("character_asscending", "");
                            Universal_Var_Cls.jsonObject.put("character_decending", "z");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        loaderIsVisible = true;
                        liqourListData(Universal_Var_Cls.bottleListUrl);
                    }
                });
            }
        });
    }

    private void setRecyclerViewData() {
        layoutManager = new GridLayoutManager(context, 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void priceMinMaxFilter() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Universal_Var_Cls.filterMinMaxPriceUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Universal_Var_Cls.minPriceFilter = response.getInt("min");
                    Universal_Var_Cls.maxPriceFilter = response.getInt("max");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void barList() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Universal_Var_Cls.filterBarUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Universal_Var_Cls.filterBarList = new FilterCategoryList();
                Universal_Var_Cls.filterBarList = new Gson().fromJson(response.toString(), FilterCategoryList.class);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void categoryList() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Universal_Var_Cls.filterCategoryUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Universal_Var_Cls.filterCategoryList = new FilterCategoryList();
                Universal_Var_Cls.filterCategoryList = new Gson().fromJson(response.toString(), FilterCategoryList.class);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }


    private void paginationMethod() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                System.out.println("------- total items ------ " + totalItems);
                System.out.println("------- current  items ------ " + currentItems);
                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        System.out.println("--------- next page --------- " + nextPage);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isPaginate = true;
                                liqourListData(nextPage);
                            }
                        }, 100);
                    }
                }
            }
        });
    }

    private void liqourListData(final String url) {

        if (Universal_Var_Cls.dialog.isShowing()) {

        } else if (loaderIsVisible) {
            Universal_Var_Cls.dialog.show();
        }

        Log.d("call_back_s", "after_restart_OnStart");
        Log.d("call_back_json", Universal_Var_Cls.jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, Universal_Var_Cls.jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("volley_response_liqour", "Success\n" + response.toString());

                if (Universal_Var_Cls.dialog.isShowing()) {
                    Universal_Var_Cls.dialog.dismiss();
                }
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                LiquorBottleListCls liqourBottleListCls = new Gson().fromJson(response.toString(), LiquorBottleListCls.class);

                if (liqourBottleListCls.getResults() != null && !liqourBottleListCls.getResults().isEmpty()) {

                    if (liqourBottleListCls.getNext() != null) {

                        String str = liqourBottleListCls.getNext();

                        if (str.contains("localhost")) {
                            str = str.replace("localhost", Universal_Var_Cls.baseDomain);
                        }
                        nextPage = str;
                    } else {
                        nextPage = null;
                    }

                    if (boolSearch) {
                        System.out.println("---------- boolsearch is true ---- " + listBottle.size());
                        listBottle = new ArrayList<>();
                        adapter = new LiquorAdapter(context, listBottle);
                        setRecyclerViewData();
                        boolSearch = false;
                    }
                    System.out.println("----------bottle size---- " + liqourBottleListCls.getResults().size());

                    ResultBottle resultBottle;
                    for (int i = 0; i < liqourBottleListCls.getResults().size(); i++) {

                        resultBottle = new ResultBottle();

                        resultBottle.setId(liqourBottleListCls.getResults().get(i).getId());
                        resultBottle.setBar(liqourBottleListCls.getResults().get(i).getBar());
                        resultBottle.setName(liqourBottleListCls.getResults().get(i).getName());
                        resultBottle.setImage(liqourBottleListCls.getResults().get(i).getImage());
                        resultBottle.setPrice(liqourBottleListCls.getResults().get(i).getPrice());
                        resultBottle.setBottleMl(liqourBottleListCls.getResults().get(i).getBottleMl());
                        resultBottle.setCategory(liqourBottleListCls.getResults().get(i).getCategory());

                        Cursor cursor = database.query(CartHelper.TABLE_CART, null, null, null, null, null, null);

                        if (cursor != null) {

                            int qty = 0;
                            while (cursor.moveToNext()) {
                                if (cursor.getInt(0) == liqourBottleListCls.getResults().get(i).getId()) {

                                    qty = cursor.getInt(1);
                                    if (qty == 0) {
                                        database.delete(CartHelper.TABLE_CART, CartHelper.KEY_ID + "=" + cursor.getInt(0), null);
                                    } else {
                                        ContentValues contentValues = new ContentValues();
                                        contentValues.put(CartHelper.KEY_PRICE, liqourBottleListCls.getResults().get(i).getPrice());
                                        database.update(CartHelper.TABLE_CART, contentValues, CartHelper.KEY_ID + "=" + cursor.getInt(0), null);
                                    }
                                    break;
                                }
                            }
                            resultBottle.setQuantity(qty);
                        }
                        if (listBottle.size() != liqourBottleListCls.getCount())
                            listBottle.add(resultBottle);

                        adapter.notifyDataSetChanged();
                    }

                    System.out.println(" ------ old counter data ---- " + Universal_Var_Cls.counter);
//                    Universal_Var_Cls.counter = 0;
//                    for (int i = 0; i < listBottle.size(); i++) {
//                        Universal_Var_Cls.counter += listBottle.get(i).getQuantity();

                        if (Universal_Var_Cls.counter > 0) {
                            counterLayout.setVisibility(View.VISIBLE);
                            System.out.println(" ------ new counter data ---- " + Universal_Var_Cls.counter);
                            counterTxt.setText("" + Universal_Var_Cls.counter);



                        }
//                    }
                    bottleNotAvailableLayout.setVisibility(View.GONE);
                } else {
                    bottleNotAvailableLayout.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (Universal_Var_Cls.dialog.isShowing()) {
                    Universal_Var_Cls.dialog.dismiss();
                }
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
                Log.d("volley_response_liqour", error.toString() + "\n" + Universal_Var_Cls.jsonObject.toString());
                //           volleyErrorResponse(error);

                VolleyErrorCls.volleyErr(error, context);
            }
        });

        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } /*else if (boolBackSearch) {
            boolBackSearch = false;
//            prepareBottleList();
        }*/ else {
            clearFIlter();
            saveData(LiquorActivity.this, "filterCount", "0");

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            /*finish();
            System.exit(0);*/
            //           return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.liquor, menu);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchEditText.setBackgroundResource(R.drawable.back_promo_code);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);
        ImageView closeButton = (ImageView) searchView.findViewById(R.id.search_close_btn);

        closeButton.setImageResource(R.drawable.close_black);

        searchEditText.setTextColor(Color.parseColor("#000000"));
        searchEditText.setHintTextColor(Color.parseColor("#777777"));
        searchEditText.setHint("Search Liqour");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                //              searchView.setIconified(true);
                //              searchView.onActionViewCollapsed();

                boolSearch = true;
                boolBackSearch = true;
                try {
                    Universal_Var_Cls.jsonObject.put("name", s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("search_json", Universal_Var_Cls.jsonObject.toString());
                liqourListData(Universal_Var_Cls.bottleListUrl);

                //             toolbar.collapseActionView();
                /*try {
                    Universal_Var_Cls.jsonObject.put("name", "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (s.length() == 0) {
                    boolSearch = true;
                    try {
                        Universal_Var_Cls.jsonObject.put("name", "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("search_json", Universal_Var_Cls.jsonObject.toString());
                    liqourListData(Universal_Var_Cls.bottleListUrl);
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_tools) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
