package com.graffersid.mobilebar.activities;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.BottleDetailAdapter;
import com.graffersid.mobilebar.classes.CartHelper;
import com.graffersid.mobilebar.classes.CurrentLocation;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.btl_detail.BottleDetail;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class BottleDetailActivity extends BaseActivity {

    private View mContentView;
    Context context;
    BottleDetail bottleDetail;
    BottleDetailAdapter adapter;
    RecyclerView recyclerView;
    TextView bottelBrand, bottleCategory, bottlePrice, bottleQty;
    ImageButton incBtlQty, decBtlQty, backBtn;
    ImageView btlImg;
    Button addToCart;
    BottleDetailActivity bottleDetailActivity;
    boolean enableDisableAddToCart;
    private CartHelper helper;
    private SQLiteDatabase database;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bottle_detail);

        context = BottleDetailActivity.this;

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        helper = new CartHelper(context);
        database = helper.getWritableDatabase();

        mContentView = findViewById(R.id.fullscreen_content);
        /*mContentView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LOW_PROFILE |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);*/

        bottleDetailActivity = new BottleDetailActivity();

        recyclerView = (RecyclerView) findViewById(R.id.btl_detail_recycler_vw);
        bottelBrand = (TextView) findViewById(R.id.btl_name_in_detail);
        bottleCategory = (TextView) findViewById(R.id.btl_category_in_detail);
        bottlePrice = (TextView) findViewById(R.id.btl_price_in_detail);
        bottleQty = (TextView) findViewById(R.id.btl_qty_in_detail);
        incBtlQty = (ImageButton) findViewById(R.id.inc_btl_qty_in_detail);
        decBtlQty = (ImageButton) findViewById(R.id.dec_btl_qty_in_detail);
        btlImg = (ImageView) findViewById(R.id.btl_img_in_detail);
        backBtn = (ImageButton) findViewById(R.id.back_bottle_detail_btn);
        addToCart = (Button) findViewById(R.id.add_to_cart_bottle_detail);

        enableDisableAddToCart = true;
        addToCart.setEnabled(false);


        int id = Universal_Var_Cls.bottleId;

        incBtlQty.setEnabled(false);
        decBtlQty.setEnabled(false);

        bottleDetailM(id);


        incBtlQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int inc = Integer.parseInt(bottleQty.getText().toString()) + 1;

                bottleQty.setText("" + inc);

                if (enableDisableAddToCart) {
                    addToCart.setBackgroundResource(R.drawable.back_add_to_cart);
                    enableDisableAddToCart = false;
                    addToCart.setEnabled(true);
                }

                if (inc > 1) {
                    ContentValues values = new ContentValues();
                    values.put(CartHelper.KEY_ID, Universal_Var_Cls.bottleId); // Contact Name
                    values.put(CartHelper.KEY_QTY, Integer.parseInt(bottleQty.getText().toString())); // Contact Phone
                    values.put(CartHelper.KEY_NAME, Universal_Var_Cls.name);
                    values.put(CartHelper.KEY_CATEGORY, Universal_Var_Cls.category);
                    values.put(CartHelper.KEY_IMAGE, Universal_Var_Cls.image);
                    values.put(CartHelper.KEY_PRICE, Universal_Var_Cls.bottlePrice);
                    values.put(CartHelper.KEY_QTY_LIQOUR, Universal_Var_Cls.liqourQty);

                    database.update(CartHelper.TABLE_CART, values, CartHelper.KEY_ID + " = " + Universal_Var_Cls.bottleId, null);
                }else {
                    ContentValues values = new ContentValues();
                    values.put(CartHelper.KEY_ID, Universal_Var_Cls.bottleId); // Contact Name
                    values.put(CartHelper.KEY_QTY, Integer.parseInt(bottleQty.getText().toString())); // Contact Phone
                    values.put(CartHelper.KEY_NAME, Universal_Var_Cls.name);
                    values.put(CartHelper.KEY_CATEGORY, Universal_Var_Cls.category);
                    values.put(CartHelper.KEY_IMAGE, Universal_Var_Cls.image);
                    values.put(CartHelper.KEY_PRICE, Universal_Var_Cls.bottlePrice);
                    values.put(CartHelper.KEY_QTY_LIQOUR, Universal_Var_Cls.liqourQty);

                    database.insert(CartHelper.TABLE_CART, null, values);
                }
            }
        });
        decBtlQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int dec = Integer.parseInt(bottleQty.getText().toString()) - 1;

                if (dec > 0) {
                    ContentValues values = new ContentValues();
                    values.put(CartHelper.KEY_ID, Universal_Var_Cls.bottleId);
                    values.put(CartHelper.KEY_QTY, dec);
                    values.put(CartHelper.KEY_NAME, Universal_Var_Cls.name);
                    values.put(CartHelper.KEY_CATEGORY, Universal_Var_Cls.category);
                    values.put(CartHelper.KEY_IMAGE, Universal_Var_Cls.image);
                    values.put(CartHelper.KEY_PRICE, Universal_Var_Cls.bottlePrice);
                    values.put(CartHelper.KEY_QTY_LIQOUR, Universal_Var_Cls.liqourQty);

                    database.update(CartHelper.TABLE_CART, values, CartHelper.KEY_ID + " = " + Universal_Var_Cls.bottleId, null);
                } else {
                    addToCart.setBackgroundResource(R.drawable.back_add_to_cart_disable);
                    enableDisableAddToCart = true;
                    addToCart.setEnabled(false);
                    bottleQty.setText("0");

                    database.delete(CartHelper.TABLE_CART, CartHelper.KEY_ID + " = " + Universal_Var_Cls.bottleId, null);
                }

                bottleQty.setText("" + dec);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getString(Universal_Var_Cls.toKen, null) != null) {
                    startActivity(new Intent(BottleDetailActivity.this, CartActivity.class));
                } else {
                    startActivity(new Intent(BottleDetailActivity.this, SignInUpActivity.class));
                }
            }
        });
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    private void bottleDetailM(final int id) {

        if (Universal_Var_Cls.lat == 0.0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    new CurrentLocation(context).lattLong();
                    bottleDetailM(id);
                }
            }, 1500);
        } else {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("bottle_id", id);
                jsonObject.put("latx", Universal_Var_Cls.lat);
                jsonObject.put("longy", Universal_Var_Cls.lng);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.bottleDetailUrl, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    incBtlQty.setEnabled(true);
                    decBtlQty.setEnabled(true);
                    Log.d("res_v_bot_detail", response.toString());
                    bottleDetail = new Gson().fromJson(response.toString(), BottleDetail.class);

                    bottelBrand.setText(bottleDetail.getData().get(0).getName());
                    bottleCategory.setText(bottleDetail.getData().get(0).getCategory());
                    bottlePrice.setText("S$ " + Universal_Var_Cls.bottlePrice);
                    bottleQty.setText("" + Universal_Var_Cls.bottleQty);
                    Picasso.with(context).load(Universal_Var_Cls.baseUrl + bottleDetail.getData().get(0).getImage()).error(R.drawable.bottle_gray).into(btlImg);


                    adapter = new BottleDetailAdapter(context, bottleDetail.getData().get(0).getBar());
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    recyclerView.setAdapter(adapter);

                    if (Universal_Var_Cls.bottleQty == 0) {
                        addToCart.setEnabled(false);
                        addToCart.setBackgroundResource(R.drawable.back_add_to_cart_disable);
                        enableDisableAddToCart = true;
                    } else {
                        addToCart.setEnabled(true);
                        addToCart.setBackgroundResource(R.drawable.back_add_to_cart);
                        enableDisableAddToCart = false;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("res_v_bot_detail", error.toString());
                    VolleyErrorCls.volleyErr(error, context);
                }
            });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(request);
        }
    }
}
