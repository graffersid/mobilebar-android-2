package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ReferFriendActivity extends BaseActivity {

    ImageButton btnBack;
    TextView referralCode, emailSent;
    EditText mailId;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private HashMap<String, String> headers;
    private Context context;
    private Button btnRefer;
    private LinearLayout layout, layout22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_friend);

        context = ReferFriendActivity.this;
        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");


        referralCode = (TextView) findViewById(R.id.referral_code);
        emailSent = (TextView) findViewById(R.id.email_sent_txt);
        mailId = (EditText) findViewById(R.id.mail_id);
        btnRefer = (Button) findViewById(R.id.share_to_friend);
        btnBack = (ImageButton) findViewById(R.id.back_btn_refer);

        layout = (LinearLayout) findViewById(R.id.layout1);
        layout22 = (LinearLayout) findViewById(R.id.layout2);

        layout22.setVisibility(View.GONE);


        Universal_Var_Cls.loader(context);

        referCode();

        btnRefer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mailId.getText().toString().length() > 0){
                    if (Universal_Var_Cls.isValidate(mailId.getText().toString())){
                        referToFriend(mailId.getText().toString());
                    }
                }
            }
        });


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void referToFriend(final String eMail) {

        Universal_Var_Cls.dialog.show();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("email", eMail);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.referFriend, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();

                Log.d("refer_fe_s", response.toString());
                String str = "Email has been sent to "+eMail+". Once he signs up 100 points will be credited to your account.";
                emailSent.setText(str);

                layout.setVisibility(View.GONE);
                layout22.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Universal_Var_Cls.dialog.dismiss();
                Log.d("refer_fe_e", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void referCode() {

        Universal_Var_Cls.dialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Universal_Var_Cls.referCode, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();

                String refer = "";
                try {
                    refer = response.getJSONObject("data").getString("reffer_code");
                    referralCode.setText(refer);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("refer_fe_s", response.toString()+"   "+refer);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Universal_Var_Cls.dialog.dismiss();
                Log.d("refer_fe_e", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
