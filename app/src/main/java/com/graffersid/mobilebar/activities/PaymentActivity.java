package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.CartHelper;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PaymentActivity extends BaseActivity {

    private CardMultilineWidget cardWidget;
    private Button btnCardPay;
    private Context context;
    private Stripe stripe;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private HashMap<String, String> headers;
    private String stripe_token;
    private CartHelper helper;
    private SQLiteDatabase database;
    private ImageButton btnBack;
    private String strCard, strVal, strCVC;
    private String publishableKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        strCard = "";
        strVal = "";
        strCVC = "";
        publishableKey = "";

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        stripe_token = "";

        btnCardPay = (Button) findViewById(R.id.card_payment);
        btnBack = (ImageButton) findViewById(R.id.back_btn_payment);

        cardWidget = (CardMultilineWidget) findViewById(R.id.card_multiline_widget);

        context = PaymentActivity.this;
        helper = new CartHelper(context);
        database = helper.getWritableDatabase();

        btnCardPay.setEnabled(false);

        btnCardPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Universal_Var_Cls.loader(context);
                Universal_Var_Cls.dialog.show();

                if (publishableKey.length() > 0) {
                    onCardSaved();
                }
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cardWidget.setCardNumberTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                strCard = charSequence.toString();
                cardCheck(strCard, strVal, strCVC);
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        cardWidget.setExpiryDateTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                strVal = charSequence.toString();
                cardCheck(strCard, strVal, strCVC);
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        cardWidget.setCvcNumberTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                strCVC = charSequence.toString();
                cardCheck(strCard, strVal, strCVC);
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        adminSettingApi();
    }

    private void adminSettingApi() {

        String url = Universal_Var_Cls.adminSetting;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_bottle_save_", response.toString());

                Universal_Var_Cls.dialog.dismiss();

                try {
                    publishableKey = response.getString("publish_ky");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();

                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }

    private void cardCheck(String strCard, String strVal, String strCVC) {

        int lenCard = strCard.length();
        int lenVal = strVal.length();
        int lenCVC = strCVC.length();
        if (lenCard == 19 && lenVal == 5 && lenCVC == 3){
            btnCardPay.setEnabled(true);
        }else {
            btnCardPay.setEnabled(false);
        }
    }

    private void onCardSaved() {

        Card cardToSave = cardWidget.getCard();
        if (cardToSave != null) {
            // tokenize card (see below)
            tokenizeCard(cardToSave);
        }
    }

    private void tokenizeCard(@NonNull Card card) {

        stripe = new Stripe(context, publishableKey);

        stripe.createToken(card,
                new ApiResultCallback<Token>() {
                    public void onSuccess(@NonNull Token token) {
                        // send token ID to your server, you'll create a charge next

                        Log.d("token_payment", "" + token.getId() + "     " + token.getCard().getName());

                        stripe_token = token.getId();

                        JSONObject jsonObject = null;

                        try {
                            jsonObject = new JSONObject(getIntent().getStringExtra("jsonBottlePurchase"));
                            jsonObject.put("token", stripe_token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (Universal_Var_Cls.isExtend){

                            extendValidity(jsonObject);
                        }else {
                            bottlePurchase(jsonObject);
                        }
                    }

                    @Override
                    public void onError(@NonNull Exception e) {
                        Log.d("token_payment", "" + e.toString());
                        Universal_Var_Cls.dialog.dismiss();
                    }
                }
        );
    }


    private void extendValidity(JSONObject jsonObject){

        Log.d("resp_bottle_save_", jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.extendBottleValidity, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_bottle_save_", response.toString());

                Universal_Var_Cls.dialog.dismiss();
                editor.remove(Universal_Var_Cls.bottlePurchasedId);
                editor.commit();

                Intent intent = new Intent(PaymentActivity.this, MyBarActivity.class);

                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();

                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }


    private void bottlePurchase(JSONObject jsonObject) {

        Log.d("resp_bottle_save_", jsonObject.toString());

        String url = Universal_Var_Cls.bottlePurchase;

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

//                Universal_Var_Cls.isRenew = !Universal_Var_Cls.isRenew;

                Universal_Var_Cls.dialog.dismiss();
                Log.d("resp_bottle_save_s", response.toString());
                database.delete(CartHelper.TABLE_CART, null, null);

                startActivity(new Intent(PaymentActivity.this, MyBarActivity.class));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }
}
