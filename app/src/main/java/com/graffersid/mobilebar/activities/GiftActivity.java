package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class GiftActivity extends BaseActivity {

    ImageButton btnBack;
    LinearLayout layout1, layout2, layout3;
    EditText emailTxt;
    Button btnGift1, btnGift22;
    TextView txtMailId, txtTransferred;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift);

        context = GiftActivity.this;
        initialize();
    }

    private void initialize() {

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        btnBack = (ImageButton) findViewById(R.id.back_btn_gift);

        layout1 = (LinearLayout) findViewById(R.id.layout1);
        layout2 = (LinearLayout) findViewById(R.id.layout2);
        layout3 = (LinearLayout) findViewById(R.id.layout3);

        emailTxt = (EditText) findViewById(R.id.email_txt);
        btnGift1 = (Button) findViewById(R.id.btn_gift_1);
        btnGift22 = (Button) findViewById(R.id.btn_gift_22);
        txtMailId = (TextView) findViewById(R.id.txt_mail_id);
        txtTransferred = (TextView) findViewById(R.id.txt_transferred);

        layout1.setVisibility(View.VISIBLE);
        layout2.setVisibility(View.GONE);
        layout3.setVisibility(View.GONE);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnGift1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (emailTxt.getText() != null && emailTxt.getText().toString().length() > 0){
                    if (Universal_Var_Cls.isValidate(emailTxt.getText().toString())){

                        layout1.setVisibility(View.GONE);
                        layout2.setVisibility(View.VISIBLE);

                        txtMailId.setText(emailTxt.getText().toString());
                    }
                }
            }
        });
        btnGift22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Universal_Var_Cls.loader(GiftActivity.this);
                Universal_Var_Cls.dialog.show();

                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("email", txtMailId.getText().toString());
                    jsonObject.put("purchased_id", preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0));
                    jsonObject.put("date", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("Gift_json : " ,jsonObject.toString());

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.sendingGift, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Universal_Var_Cls.dialog.dismiss();

                        editor.remove(Universal_Var_Cls.bottlePurchasedId);
                        editor.commit();

                        try {
                            if (response.getBoolean("status")){

                                //Toast.makeText(GiftActivity.this, "", Toast.LENGTH_SHORT).show();

                                layout2.setVisibility(View.GONE);
                                layout3.setVisibility(View.VISIBLE);

                                String trnsfrTxt = "Bottle has been gifted to your friend email id."
                                        +"\nAsk your friend to sign in to avail the gift.";
                                txtTransferred.setText(trnsfrTxt);
                     //           startActivity(new Intent(GiftActivity.this, MyBarActivity.class));
                            }else {
                                layout2.setVisibility(View.GONE);
                                layout3.setVisibility(View.VISIBLE);
                                String trnsfrTxt = "Enter email id is new to MobileBar therefore 100% of the bottle is transferred to the entered email id."
                                        +"\nAsk your friend to sign up to avail the gift.";
                                txtTransferred.setText(trnsfrTxt);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Universal_Var_Cls.dialog.dismiss();
                        VolleyErrorCls.volleyErr(error, context);
                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                        headers.put("Content-Type", "application/json");
                        return headers;
                    }
                };
                int timeOut = 15000;
                DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                request.setRetryPolicy(retryPolicy);

                RequestQueue queue = Volley.newRequestQueue(GiftActivity.this);
                queue.add(request);
            }
        });
    }


    @Override
    public void onBackPressed() {

        if (layout2.getVisibility() == View.VISIBLE){
            layout2.setVisibility(View.GONE);
            layout1.setVisibility(View.VISIBLE);
        }else {

            if (preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0) != 0) {
                editor.remove(Universal_Var_Cls.bottlePurchasedId);
                editor.commit();
            }
            super.onBackPressed();

        //    startActivity(new Intent(GiftActivity.this, MyBarActivity.class));
        }
    }
}
