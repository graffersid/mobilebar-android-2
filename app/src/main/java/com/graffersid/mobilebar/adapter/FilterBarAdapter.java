package com.graffersid.mobilebar.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.FilterActivity;
import com.graffersid.mobilebar.activities.LiquorActivity;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.filter_category_list.ResultCategory;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.graffersid.mobilebar.classes.MySharedPref.saveData;
import static com.graffersid.mobilebar.classes.Universal_Var_Cls.listBar;

/**
 * Created by Sandy on 31/07/2019 at 02:15 PM.
 */
public class FilterBarAdapter extends RecyclerView.Adapter<FilterBarAdapter.VwHolder> {

    Context context;
    LayoutInflater inflater;

    public FilterBarAdapter(Context context, List<ResultCategory> listBar) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.item_filter_bar, parent, false);
        return new VwHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VwHolder holder, final int position) {

        if (listBar.get(position).getImage() != null){

            String str = listBar.get(position).getImage();

            if (str.contains("localhost")) {
                str = str.replace("localhost", Universal_Var_Cls.baseDomain);
            }
            Picasso.with(context).load(str).fit().centerCrop().placeholder(R.drawable.ic_bar_default_img)
                    .into(holder.imageView);
        } else
            Picasso.with(context).load(Universal_Var_Cls.barImgUrl )
                    .placeholder(R.drawable.ic_bar_default_img).into(holder.imageView);



        holder.barName.setText(listBar.get(position).getName());
        holder.barAddress.setText(listBar.get(position).getAddress());

        if (listBar.get(position).getIsSelect()){
            holder.barSelected.setVisibility(View.VISIBLE);
        }else {holder.barSelected.setVisibility(View.GONE);}

        holder.barLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResultCategory resultCategory = new ResultCategory();
                resultCategory.setId(listBar.get(position).getId());
                resultCategory.setName(listBar.get(position).getName());
                resultCategory.setImage(listBar.get(position).getImage());
                resultCategory.setAddress(listBar.get(position).getAddress());
                resultCategory.setIsSelect(!listBar.get(position).getIsSelect());
                listBar.set(position, resultCategory);

                if (listBar.get(position).getIsSelect()){
                    holder.barSelected.setVisibility(View.VISIBLE);
                }else {holder.barSelected.setVisibility(View.GONE);}

                if (listBar.get(position).getIsSelect()) {
                    Universal_Var_Cls.filterBarIdList.set(position, String.valueOf(listBar.get(position).getId()));
                }else {
                    Universal_Var_Cls.filterBarIdList.set(position, "-1");
                }

                int countBar = 0;
                for (int i = 0; i<Universal_Var_Cls.filterBarIdList.size(); i++){

                    if (Universal_Var_Cls.filterBarIdList.get(i) != "-1") {
                        countBar += 1;
                    }
                }
                FilterActivity.tabLayout.getTabAt(1).setText("BAR("+countBar+")");

                for (int i = 0; i<Universal_Var_Cls.filterCategoryIdList.size(); i++){

                    if (Universal_Var_Cls.filterCategoryIdList.get(i) != "-1") {
                        countBar += 1;
                    }
                }

                if (countBar > 0) {
                    saveData(context,"filterCount" ,String.valueOf((countBar)));

                    LiquorActivity.filterBtn.setText("Filter(" + (countBar) + ")");
                }else {
                    LiquorActivity.filterBtn.setText("Filter");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listBar.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView barName, barAddress;
        LinearLayout barLayout;
        ImageView barSelected;
        public VwHolder(@NonNull View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.filter_bar_image);
            barName = (TextView) itemView.findViewById(R.id.filter_bar_name);
            barAddress = (TextView) itemView.findViewById(R.id.filter_bar_address);
            barLayout = (LinearLayout) itemView.findViewById(R.id.bar_layout);
            barSelected = (ImageView) itemView.findViewById(R.id.img_bar_selected);
        }
    }
}
