package com.graffersid.mobilebar.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.HistoryActivity;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Sandy on 01/10/2019 at 04:43 PM.
 */
public class BottleHistoryAdapter extends RecyclerView.Adapter<BottleHistoryAdapter.VwHolder> {

    Context context;
    List<Result> listHistoryBottle;
    LayoutInflater inflater;

    public BottleHistoryAdapter(Context context, List<Result> listHistoryBottle) {
        this.context = context;
        this.listHistoryBottle = listHistoryBottle;

        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_by_bar_bottle_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, final int position) {

        if (listHistoryBottle.get(position).getImage() != null){
            Picasso.with(context).load(Universal_Var_Cls.baseUrl+listHistoryBottle.get(position).getImage()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(holder.imgBottle);
        }else {
            holder.imgBottle.setImageResource(R.drawable.bottle_gray);
        }
        holder.nameBottle.setText(listHistoryBottle.get(position).getName());

        try {
            String purchaseDate ;
            if (listHistoryBottle.get(position).getIsGifted()) {
                purchaseDate = "Gifted: " + new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yy/MMMM/dd").parse(listHistoryBottle.get(position).getCreateDate()));
                holder.giftedSign.setVisibility(View.VISIBLE);

            } else {
                holder.giftedSign.setVisibility(View.GONE);

                purchaseDate = "Purchased: " + new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yy/MMMM/dd").parse(listHistoryBottle.get(position).getCreateDate()));
            }

            String expiryDate = "Till: "+new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yy/MMMM/dd").parse(listHistoryBottle.get(position).getExpiry()));


            holder.datePurchased.setText(purchaseDate);
            holder.dateExpiry.setText(expiryDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.btnViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Universal_Var_Cls.bottleIdPurchased = listHistoryBottle.get(position).getId();
                HistoryActivity activity = (HistoryActivity) context;
                activity.startBottleHistory();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listHistoryBottle.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        ImageView imgBottle,giftedSign;
        TextView nameBottle, datePurchased, dateExpiry;
        Button btnViewMore;
        CardView cardView;
        public VwHolder(@NonNull View itemView) {
            super(itemView);

            imgBottle = (ImageView) itemView.findViewById(R.id.img_bar);
            nameBottle = (TextView) itemView.findViewById(R.id.bar_name);
            datePurchased = (TextView) itemView.findViewById(R.id.bar_distance);
            dateExpiry = (TextView) itemView.findViewById(R.id.consumed_liqour);
            btnViewMore = (Button) itemView.findViewById(R.id.no_of_bottle);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            giftedSign =itemView.findViewById(R.id.giftedSign);

            imgBottle.setScaleType(ImageView.ScaleType.FIT_CENTER);

            btnViewMore.setText("View More");

            cardView.setElevation(0);
            cardView.setCardElevation(0);
        }
    }
}
