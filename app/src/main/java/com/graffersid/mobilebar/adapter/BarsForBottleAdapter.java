package com.graffersid.mobilebar.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.ConsumeLiquorActivity;
import com.graffersid.mobilebar.activities.MobileOtpVerifyActivity;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.bars.Bar;
import com.graffersid.mobilebar.model_cls.profile.ProfileModelCls;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Sandy on 04/10/2019 at 03:33 PM.
 */
public class BarsForBottleAdapter extends RecyclerView.Adapter<BarsForBottleAdapter.VwHolder> implements Filterable {

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    Context context;
    List<Bar> list;
    List<Bar> listFull;
    LayoutInflater inflater;
    private HashMap<String, String> headers;

    public BarsForBottleAdapter(Context context, List<Bar> list) {
        this.context = context;
        this.list = list;

        listFull = new ArrayList<>(list);
        inflater = LayoutInflater.from(context);

        preferences = context.getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();
        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_bars_for_bottle, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, final int position) {

        if (list.get(position).getImage() != null){
            Picasso.with(context).load(Universal_Var_Cls.baseUrl+list.get(position).getImage()).placeholder(R.drawable.ic_bar).error(R.drawable.ic_bar).into(holder.imageView);
        }else {
            holder.imageView.setImageResource(R.drawable.ic_bar);
        }
        holder.barName.setText(list.get(position).getName());
        holder.barPrice.setText("$ "+list.get(position).getPrice());

        int rate = (int) Math.ceil(30*list.get(position).getConversionMl());
        String str = "30ml at Bar = <font color=#FF1C1C>"+rate+"ml</font> of bottle";
        holder.barRate.setText(Html.fromHtml(str));

        holder.btnConsume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Universal_Var_Cls.barId = list.get(position).getId();
                checkMobileVerified();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView barName, barDistance, barPrice, barRate;
        Button btnConsume;
        public VwHolder(@NonNull View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.img_bar);
            barName = (TextView) itemView.findViewById(R.id.bar_name);
            barDistance = (TextView) itemView.findViewById(R.id.bar_discount);
            barPrice = (TextView) itemView.findViewById(R.id.bar_price);
            barRate = (TextView) itemView.findViewById(R.id.bar_rate);
            btnConsume = (Button) itemView.findViewById(R.id.btn_consume);
        }
    }

    @Override
    public Filter getFilter() {
        return searchFilter;
    }
    private Filter searchFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Bar> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Bar item : listFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }/*else if (item.getMob().contains(filterPattern)){
                        filteredList.add(item);
                    }*/
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            list.clear();
            list.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    private void checkMobileVerified() {


        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Universal_Var_Cls.profileDetail, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                ProfileModelCls profileModelCls = null;
                try {
                    profileModelCls = new Gson().fromJson(response.getJSONObject(0).toString(), ProfileModelCls.class);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jsonObject = response.getJSONObject(0);

                    try {
                        if (jsonObject.getBoolean("verified") == false){

                            if (profileModelCls.getPin() != null) {
                                Universal_Var_Cls.pinCustomer = Integer.parseInt(profileModelCls.getPin());
  //                              Universal_Var_Cls.pinCustomer = 1234;
                            }
                            context.startActivity(new Intent(context, MobileOtpVerifyActivity.class));
                        }else if (profileModelCls.getPin() != null) {
                            Universal_Var_Cls.pinCustomer = Integer.parseInt(profileModelCls.getPin());
 //                           Universal_Var_Cls.pinCustomer = 1234;
                            context.startActivity(new Intent(context, ConsumeLiquorActivity.class));
                        }else {
                            context.startActivity(new Intent(context, MobileOtpVerifyActivity.class));
                        }

                        if (profileModelCls.getPhone() != null){

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("profile_resp_success", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("profile_resp_error", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
