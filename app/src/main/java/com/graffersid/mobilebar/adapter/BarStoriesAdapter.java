package com.graffersid.mobilebar.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.CartActivity;
import com.graffersid.mobilebar.activities.ConsumeLiquorActivity;
import com.graffersid.mobilebar.activities.MobileOtpVerifyActivity;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.BottleDetail;
import com.graffersid.mobilebar.model_cls.profile.ProfileModelCls;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Sandy on 24/09/2019 at 12:24 PM.
 */
public class BarStoriesAdapter extends RecyclerView.Adapter<BarStoriesAdapter.VwHolder> implements Filterable {

    private TextView txtColor;
    private TextView txtColorMargin;
    private LinearLayout layoutColorBar;
    private TextView txtQty;
    private TextView txtQtyMargin;
    private RelativeLayout layoutQtyBack;
    private LinearLayout layoutQtyBar;
    private HashMap<String, String> headers;
    Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private DisplayMetrics displayMetrics;
    private int height;
    private int width;
    DecimalFormat df2;

    List<BottleDetail> listBottle;
    List<BottleDetail> listFull;

    Drawable wrappedDrawable;

    public BarStoriesAdapter(Context context, List<BottleDetail> listBottle) {

        this.context = context;
        this.listBottle = listBottle;

        listFull = new ArrayList<>(listBottle);

        preferences = context.getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        df2 = new DecimalFormat("#.##");

        displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(context, R.drawable.back_liquor_qty);
        wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(LayoutInflater.from(context).inflate(R.layout.item_by_bottle, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, final int position) {

        float remain;
        float sum;
        if (listBottle.get(position).getTotalMl() != null) {
            if (listBottle.get(position).getRemainingMl() != null) {

                sum = Float.parseFloat(String.valueOf(Math.ceil(listBottle.get(position).getTotalMl())));
                remain = Float.parseFloat(String.valueOf(Math.ceil(listBottle.get(position).getRemainingMl())));
                layoutQtyBar.setWeightSum(sum);
                layoutColorBar.setWeightSum(sum);
                setColorMethod(remain, sum);
            }
        }

        holder.brandName.setText(listBottle.get(position).getName());
        holder.noOfBottles.setText("No of bottles: "+listBottle.get(position).getPurchasedBottle());

        if (listBottle.get(position).getImage() != null) {

            Picasso.with(context).load(Universal_Var_Cls.baseUrl + listBottle.get(position).getImage()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(holder.imgBottle);
        }

        holder.btnRenew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Universal_Var_Cls.bottleId = listBottle.get(position).getBottleId();
                Universal_Var_Cls.nameBottleRenew = listBottle.get(position).getName();
                Universal_Var_Cls.imgBottleRenew = listBottle.get(position).getImage();
                Universal_Var_Cls.brandNameBottleRenew = listBottle.get(position).getCategory();
                Universal_Var_Cls.priceBottleRenew = listBottle.get(position).getPrice();
                Universal_Var_Cls.qtyLiqourBottleRenew = listBottle.get(position).getTotalMl()/listBottle.get(position).getPurchasedBottle();

                Universal_Var_Cls.isRenew = true;

                context.startActivity(new Intent(context, CartActivity.class));
            }
        });

        holder.btnConsume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Universal_Var_Cls.customerBottleId = listBottle.get(position).getCustomerBottleId();
                Universal_Var_Cls.bottleId = listBottle.get(position).getBottleId();
                checkMobileVerified();
            }
        });
    }


    @Override
    public int getItemCount() {
        return listBottle.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        LinearLayout layout, layout_gift_btn;
        TextView brandName, noOfBottles;
        ImageView imgBottle;
        Button btnRenew, btnConsume;
        ImageButton btnGift;
        public VwHolder(@NonNull View itemView) {
            super(itemView);

            layout = (LinearLayout) itemView.findViewById(R.id.item_layout_bottle_list);
            layout_gift_btn = (LinearLayout) itemView.findViewById(R.id.layout_gift_btn);

           /* RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) layout.getLayoutParams();
            params.height = params.height + 40;
            layout.setLayoutParams(params);*/

            layoutQtyBar = (LinearLayout) itemView.findViewById(R.id.liqour_qty_bar);
            layoutQtyBack = (RelativeLayout) itemView.findViewById(R.id.liqour_qty_back);
            txtQtyMargin = (TextView) itemView.findViewById(R.id.liqour_qty_margin);
            txtQty = (TextView) itemView.findViewById(R.id.liqour_qty);

            layoutColorBar = (LinearLayout) itemView.findViewById(R.id.liqour_bar);
            txtColorMargin = (TextView) itemView.findViewById(R.id.liqour_margin_color_bar);
            txtColor = (TextView) itemView.findViewById(R.id.liqour_color_bar);


            brandName = (TextView) itemView.findViewById(R.id.brand_name);
            noOfBottles = (TextView) itemView.findViewById(R.id.no_of_bottle);
            imgBottle = (ImageView) itemView.findViewById(R.id.img_bottle);

            btnRenew = (Button) itemView.findViewById(R.id.btn_extend_validity);
            btnConsume = (Button) itemView.findViewById(R.id.btn_no_of_bar);
            btnGift = (ImageButton) itemView.findViewById(R.id.btn_img_gift);

            layout_gift_btn.setVisibility(View.GONE);
            btnRenew.setText("Renew");
            btnConsume.setText("Consume");
        }
    }

    @Override
    public Filter getFilter() {
        return searchFilter;
    }
    private Filter searchFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<BottleDetail> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (BottleDetail item : listFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }/*else if (item.getMob().contains(filterPattern)){
                        filteredList.add(item);
                    }*/
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            listBottle.clear();
            listBottle.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    private void checkMobileVerified() {


        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Universal_Var_Cls.profileDetail, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                ProfileModelCls profileModelCls = null;
                try {
                    profileModelCls = new Gson().fromJson(response.getJSONObject(0).toString(), ProfileModelCls.class);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jsonObject = response.getJSONObject(0);

                    try {
                        if (jsonObject.getBoolean("verified") == false){

                            Universal_Var_Cls.pinCustomer = Integer.parseInt(profileModelCls.getPin());
 //                           Universal_Var_Cls.pinCustomer = 1234;
                            context.startActivity(new Intent(context, MobileOtpVerifyActivity.class));
                        }else {
                            Universal_Var_Cls.pinCustomer = Integer.parseInt(profileModelCls.getPin());
//                            Universal_Var_Cls.pinCustomer = 1234;
                            context.startActivity(new Intent(context, ConsumeLiquorActivity.class));
                        }

                        if (profileModelCls.getPhone() != null){

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("profile_resp_success", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("profile_resp_error", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }



    private void setColorMethod(float remain, float sum) {

        float marginBar = sum - remain;
        float limitBar = sum/100;

        if (remain >= (limitBar*75)){

            DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.liqour_green));
            layoutQtyBack.setBackgroundResource(0);
            layoutQtyBack.setBackgroundResource(R.drawable.back_liquor_qty);
            txtQty.setTextColor(context.getResources().getColor(R.color.liqour_green_txt));
            txtQty.setText(""+(int)remain+"ml left");

            txtQtyMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColorMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColor.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, remain));

            if (marginBar == 0){
                txtQtyMargin.setVisibility(View.GONE);
                txtColorMargin.setVisibility(View.GONE);
            }else {
                txtQtyMargin.setVisibility(View.VISIBLE);
                txtColorMargin.setVisibility(View.VISIBLE);
            }
        }else if (remain >= (limitBar*35)){
            DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.liqour_yellow));
            layoutQtyBack.setBackgroundResource(0);
            layoutQtyBack.setBackgroundResource(R.drawable.back_liquor_qty);
            txtQty.setTextColor(context.getResources().getColor(R.color.liqour_yellow_txt));
            txtQty.setText(""+(int)remain+"ml left");

            txtQtyMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));

            txtColorMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColor.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, remain));
        }else if (remain < (limitBar*35)){

            DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.liqour_red));
            layoutQtyBack.setBackgroundResource(0);
            layoutQtyBack.setBackgroundResource(R.drawable.back_liquor_qty);
            txtQty.setTextColor(context.getResources().getColor(R.color.liqour_red_txt));
            txtQty.setText(""+(int)remain+"ml left");

            txtQtyMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));

            txtColorMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColor.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, remain));
        }
    }
}
