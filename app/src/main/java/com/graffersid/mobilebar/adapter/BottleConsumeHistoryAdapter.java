package com.graffersid.mobilebar.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.model_cls.bottle_history.Orderdrink;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Sandy on 03/10/2019 at 03:22 PM.
 */
public class BottleConsumeHistoryAdapter extends RecyclerView.Adapter<BottleConsumeHistoryAdapter.VwHolder> {

    Context context;
    List<Orderdrink> list;
    LayoutInflater inflater;

    public BottleConsumeHistoryAdapter(Context context, List<Orderdrink> list) {

        this.context = context;
        this.list = list;

        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_bottle_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, int position) {

        holder.bar.setText(list.get(position).getBar());
        try {
            holder.date.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(list.get(position).getOrderdrinkdate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.consume.setText(""+list.get(position).getConsumedMl()+"ml");
        holder.chargeable.setText(""+list.get(position).getChargableMl()+"ml");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        TextView bottle, bar, date, consume, chargeable;
        public VwHolder(@NonNull View itemView) {
            super(itemView);

            bottle = (TextView) itemView.findViewById(R.id.consume_bottle);
            bar = (TextView) itemView.findViewById(R.id.consume_bar);
            date = (TextView) itemView.findViewById(R.id.consume_date);
            consume = (TextView) itemView.findViewById(R.id.consume_ml);
            chargeable = (TextView) itemView.findViewById(R.id.consume_charge);
        }
    }
}
