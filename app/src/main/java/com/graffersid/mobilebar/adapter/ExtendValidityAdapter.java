package com.graffersid.mobilebar.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.PaymentActivity;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.my_bar.Datum;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Sandy on 18/09/2019 at 03:31 PM.
 */
public class ExtendValidityAdapter extends RecyclerView.Adapter<ExtendValidityAdapter.VwHolder> {

    Context context;
    private List<Datum> list;
    LayoutInflater inflater;
    boolean boolDefaultSelected;
    Button lastSelectedBtn;
    CardView lastSelected;


    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public ExtendValidityAdapter(Context context, List<Datum> list, boolean b) {

        this.context = context;
        this.list = list;
        this.boolDefaultSelected = b;
        lastSelected = null;
        lastSelectedBtn = null;

        preferences = context.getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_extend_validity, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final VwHolder holder, final int position) {

        double d = Universal_Var_Cls.bottlePrice*list.get(position).getPrice()/100;

        Log.d("extend_price", ""+(d*Universal_Var_Cls.taxDefault/100));
        d = d + d*Universal_Var_Cls.taxDefault/100;

        Log.d("extend_price", ""+d);

        String price = new DecimalFormat("#.##").format(d);

        holder.extend_price.setText("S$ "+price);

        holder.txt_month_year.setText(""+list.get(position).getName());
        holder.layoutCardVw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lastSelected != null){
                    if (lastSelected == (CardView) view){

                    }else {
                        lastSelectedBtn.setBackgroundResource(R.drawable.back_add_to_cart_white);
                        lastSelectedBtn.setTextColor(context.getResources().getColor(R.color.heading_liqour));
                    }
                }

                lastSelectedBtn = holder.btnExtValidity;
                lastSelected = (CardView) view;
                holder.btnExtValidity.setBackgroundResource(R.drawable.back_add_to_cart);
                lastSelectedBtn.setTextColor(context.getResources().getColor(R.color.white));
            }
        });


        holder.btnExtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*new Universal_Var_Cls().loader(context);
                Universal_Var_Cls.dialog.show();*/

                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("extend_id", list.get(position).getId());
                    jsonObject.put("purchased_id", preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0));

                    String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                    jsonObject.put("created_date_time", date);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("json_ext_validity", jsonObject.toString());
                Universal_Var_Cls.isExtend = true;
                Intent intent = new Intent(context, PaymentActivity.class);
                intent.putExtra("jsonBottlePurchase", jsonObject.toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        private final DisplayMetrics displayMetrics;
        private int width;
        private int height;
        CardView layoutCardVw;
        TextView txt_month_year, extend_price;
        Button btnExtValidity;

        public VwHolder(@NonNull View itemView) {
            super(itemView);

            displayMetrics = new DisplayMetrics();

            WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

            height = Math.round(displayMetrics.heightPixels);
            width = Math.round(displayMetrics.widthPixels);
            width = width / 3;


            layoutCardVw = (CardView) itemView.findViewById(R.id.linear_layout_ext);

            ViewGroup.LayoutParams layoutParams = layoutCardVw.getLayoutParams();
            layoutParams.width = width;
            layoutCardVw.setLayoutParams(layoutParams);

            txt_month_year = (TextView) itemView.findViewById(R.id.txt_month_year);
            extend_price = (TextView) itemView.findViewById(R.id.extend_price);
            btnExtValidity = (Button) itemView.findViewById(R.id.extend_validity_btn);
        }
    }
}
/*JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.extendBottleValidity, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("resp_ext_validity", response.toString());

                        Universal_Var_Cls.dialog.dismiss();
                        editor.remove(Universal_Var_Cls.bottlePurchasedId);
                        editor.commit();
                        context.startActivity(new Intent(context, MyBarActivity.class));
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Universal_Var_Cls.dialog.dismiss();
                        Log.d("resp_ext_validity", error.toString()+"  "+error.getStackTrace());
                        Log.d("resp_ext_validity", preferences.getString(Universal_Var_Cls.toKen, null));
                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                        headers.put("Content-Type", "application/json");
                        return headers;
                    }
                };
                int timeOut = 15000;
                DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                request.setRetryPolicy(retryPolicy);

                RequestQueue queue = Volley.newRequestQueue(context);
                queue.add(request);*/