package com.graffersid.mobilebar.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.btl_detail.Bar;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Sandy on 02/08/2019 at 04:04 PM.
 */
public class BottleDetailAdapter extends RecyclerView.Adapter<BottleDetailAdapter.VwHolder> {

    Context context;
    List<Bar> listBar;
    LayoutInflater inflater;

    public BottleDetailAdapter(Context context, List<Bar> listBar) {
        this.context = context;
        this.listBar = listBar;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.item_bottle_detail, parent, false);
        return new VwHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, int position) {
        holder.barName.setText(listBar.get(position).getName());
        holder.barAddress.setText(listBar.get(position).getAdress());
        holder.barDistance.setText(String.format("%.2f", listBar.get(position).getDistance())+" km away");
        Picasso.with(context).load(Universal_Var_Cls.baseUrl+listBar.get(position).getImage()).error(R.drawable.bottle_gray).into(holder.barImg);
    }

    @Override
    public int getItemCount() {
        return listBar.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {
        TextView barName, barAddress, barDistance;
        ImageView barImg;
        public VwHolder(@NonNull View itemView) {
            super(itemView);

            barImg = (ImageView) itemView.findViewById(R.id.btl_detail_bar_img);
            barName = (TextView) itemView.findViewById(R.id.btl_detail_bar_name);
            barAddress = (TextView) itemView.findViewById(R.id.btl_detail_bar_address);
            barDistance = (TextView) itemView.findViewById(R.id.btl_detail_bar_distance);
        }
    }
}
