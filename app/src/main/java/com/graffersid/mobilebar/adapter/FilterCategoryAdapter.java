package com.graffersid.mobilebar.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.FilterActivity;
import com.graffersid.mobilebar.activities.LiquorActivity;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.filter_category_list.ResultCategory;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.graffersid.mobilebar.classes.MySharedPref.saveData;
import static com.graffersid.mobilebar.classes.Universal_Var_Cls.listcategory;

/**
 * Created by Sandy on 30/07/2019 at 04:17 PM.
 */
public class FilterCategoryAdapter extends RecyclerView.Adapter<FilterCategoryAdapter.VwHolder> {

    Context context;
    LayoutInflater inflater;

    public FilterCategoryAdapter(Context context, List<ResultCategory> listcategory) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.item_filter_category, parent, false);
        return new VwHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VwHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (listcategory.get(position).getImage() != null) {

            String str = listcategory.get(position).getImage();

            if (str.contains("localhost")) {
                str = str.replace("localhost", Universal_Var_Cls.baseDomain);
            }

            System.out.println("----- image cat ------ "+position+"  " + str);
            Picasso.with(context).load(str).fit().centerCrop().placeholder(R.drawable.bottle_gray)
                    .into(holder.imageView);
        } else {
            System.out.println("----- image cat ------ "+position+"  " + Universal_Var_Cls.catImgUrl);
            Picasso.with(context).load(Universal_Var_Cls.catImgUrl).into(holder.imageView);
        }
        holder.textView.setText(listcategory.get(position).getName());

        if (listcategory.get(position).getIsSelect()) {
            holder.categorySelected.setVisibility(View.VISIBLE);
        } else {
            holder.categorySelected.setVisibility(View.GONE);
        }

        holder.categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ResultCategory resultCategory = new ResultCategory();

                resultCategory.setId(listcategory.get(position).getId());
                resultCategory.setName(listcategory.get(position).getName());
                resultCategory.setImage(listcategory.get(position).getImage());
                resultCategory.setIsSelect(!listcategory.get(position).getIsSelect());
                listcategory.set(position, resultCategory);

                if (listcategory.get(position).getIsSelect()) {
                    holder.categorySelected.setVisibility(View.VISIBLE);
                } else {
                    holder.categorySelected.setVisibility(View.GONE);
                }

                if (listcategory.get(position).getIsSelect()) {
                    Universal_Var_Cls.filterCategoryIdList.set(position, String.valueOf(listcategory.get(position).getId()));
                } else {
                    Universal_Var_Cls.filterCategoryIdList.set(position, "-1");
                }
                int countCat = 0;
                for (int i = 0; i<Universal_Var_Cls.filterCategoryIdList.size(); i++){

                    if (Universal_Var_Cls.filterCategoryIdList.get(i) != "-1") {
                        countCat += 1;
                    }
                }
                FilterActivity.tabLayout.getTabAt(0).setText("CATEGORY("+countCat+")");

                for (int i = 0; i<Universal_Var_Cls.filterBarIdList.size(); i++){

                    if (Universal_Var_Cls.filterBarIdList.get(i) != "-1") {
                        countCat += 1;
                    }
                }
                if (countCat > 0) {
                    saveData(context,"filterCount" ,String.valueOf((countCat)));
                    LiquorActivity.filterBtn.setText("Filter(" + (countCat) + ")");
                }else {
                    LiquorActivity.filterBtn.setText("Filter");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listcategory.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        LinearLayout categoryLayout;
        ImageView categorySelected;

        public VwHolder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.bottle_img);
            textView = (TextView) itemView.findViewById(R.id.filter_category_name);
            categoryLayout = (LinearLayout) itemView.findViewById(R.id.category_layout);
            categorySelected = (ImageView) itemView.findViewById(R.id.category_selected);
        }
    }
}
