package com.graffersid.mobilebar.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.BottleDetailActivity;
import com.graffersid.mobilebar.activities.LiquorActivity;
import com.graffersid.mobilebar.activities.SplashActivity;
import com.graffersid.mobilebar.classes.CartHelper;
import com.graffersid.mobilebar.classes.PermissionManager;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.liquor_list.ResultBottle;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Sandy on 29/07/2019 at 03:35 PM.
 */
public class LiquorAdapter extends RecyclerView.Adapter<LiquorAdapter.VwHolder> {

    Context context;
    Activity activity;
    List<ResultBottle> listBottle;
    LayoutInflater inflater;
    SQLiteDatabase database;
    CartHelper helper;


    public LiquorAdapter(Activity context, List<ResultBottle> listBottle) {
        this.context = context;
        activity = context;
        this.listBottle = listBottle;
        inflater = LayoutInflater.from(context);

        helper = new CartHelper(context);
        database = helper.getWritableDatabase();
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.item_liquor, parent, false);
        return new VwHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VwHolder holder, final int position) {

        if(listBottle.get(position).getImage() != null) {
            Picasso.with(context).load(Universal_Var_Cls.baseUrl + listBottle.get(position).getImage()).into(holder.imgBottle);
        }else {
            holder.imgBottle.setImageResource(R.drawable.bottle_gray);
        }

        holder.bottleBrandName.setText(listBottle.get(position).getName());
        holder.liqourCategory.setText(listBottle.get(position).getCategory());
        holder.bottlePrice.setText("$ " + listBottle.get(position).getPrice());

        int id = listBottle.get(position).getId();
        int qty = listBottle.get(position).getQuantity();

        if (qty > 0) {
            holder.bottleIncDecLayout.setVisibility(View.VISIBLE);
            holder.addToCartBtn.setVisibility(View.GONE);
            holder.qtyBottle.setText("" + qty);
        }else {
            holder.bottleIncDecLayout.setVisibility(View.GONE);
            holder.addToCartBtn.setVisibility(View.VISIBLE);
            holder.qtyBottle.setText("" + qty);
        }

        if (listBottle.get(position).getBar() != null) {
            holder.availableInBars.setText("" + listBottle.get(position).getBar().size() + " Bars");
        }

        holder.addToCartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottleDetail(position);
            }
        });

        holder.addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.addToCartBtn.setVisibility(View.GONE);
                holder.bottleIncDecLayout.setVisibility(View.VISIBLE);
                holder.qtyBottle.setText("" + String.valueOf(listBottle.get(position).getQuantity() + 1));
                ResultBottle resultBottle = new ResultBottle();
                resultBottle.setBar(listBottle.get(position).getBar());

                double dBottleML = listBottle.get(position).getBottleMl();
                resultBottle.setBottleMl(dBottleML);
                resultBottle.setCategory(listBottle.get(position).getCategory());
                resultBottle.setId(listBottle.get(position).getId());
                resultBottle.setImage(listBottle.get(position).getImage());
                resultBottle.setName(listBottle.get(position).getName());

                double dPrice = listBottle.get(position).getPrice();
                resultBottle.setPrice(dPrice);
                resultBottle.setQuantity(Integer.parseInt(holder.qtyBottle.getText().toString()));
                listBottle.set(position, resultBottle);
                Universal_Var_Cls.counter++;
                LiquorActivity.counterLayout.setVisibility(View.VISIBLE);
                LiquorActivity.counterTxt.setText("" + Universal_Var_Cls.counter);

                Cursor cursor = database.query(CartHelper.TABLE_CART, null, null, null, null, null, null);

                if (cursor != null) {
                    boolean bool = false;
                    while (cursor.moveToNext()) {
                        if (cursor.getInt(0) == listBottle.get(position).getId()) {
                            bool = true;
                            break;
                        }
                    }
                    if (bool) {
                        ContentValues values = new ContentValues();
                        values.put(CartHelper.KEY_ID, listBottle.get(position).getId());
                        values.put(CartHelper.KEY_QTY, listBottle.get(position).getQuantity());
                        values.put(CartHelper.KEY_NAME, listBottle.get(position).getName());
                        values.put(CartHelper.KEY_CATEGORY, listBottle.get(position).getCategory());
                        values.put(CartHelper.KEY_IMAGE, listBottle.get(position).getImage());
                        values.put(CartHelper.KEY_PRICE, listBottle.get(position).getPrice());
                        values.put(CartHelper.KEY_QTY_LIQOUR, listBottle.get(position).getBottleMl());

                        // Inserting Row
                        database.update(CartHelper.TABLE_CART, values, CartHelper.KEY_ID + " = " + String.valueOf(listBottle.get(position).getId()), null);
                    } else {
                        ContentValues values = new ContentValues();
                        values.put(CartHelper.KEY_ID, listBottle.get(position).getId()); // Contact Name
                        values.put(CartHelper.KEY_QTY, listBottle.get(position).getQuantity()); // Contact Phone
                        values.put(CartHelper.KEY_NAME, listBottle.get(position).getName());
                        values.put(CartHelper.KEY_CATEGORY, listBottle.get(position).getCategory());
                        values.put(CartHelper.KEY_IMAGE, listBottle.get(position).getImage());
                        values.put(CartHelper.KEY_PRICE, listBottle.get(position).getPrice());
                        values.put(CartHelper.KEY_QTY_LIQOUR, listBottle.get(position).getBottleMl());

                        // Inserting Row
                        database.insert(CartHelper.TABLE_CART, null, values);
                    }
                }
                //              notifyItemChanged(position);
            }
        });
        holder.incBottleQtyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.qtyBottle.setText("" + (Integer.parseInt(holder.qtyBottle.getText().toString()) + 1));

                ResultBottle resultBottle = new ResultBottle();
                resultBottle.setQuantity(Integer.parseInt(holder.qtyBottle.getText().toString()));
                resultBottle.setBar(listBottle.get(position).getBar());
                resultBottle.setBottleMl(listBottle.get(position).getBottleMl());
                resultBottle.setCategory(listBottle.get(position).getCategory());
                resultBottle.setId(listBottle.get(position).getId());
                resultBottle.setImage(listBottle.get(position).getImage());
                resultBottle.setName(listBottle.get(position).getName());
                resultBottle.setPrice(listBottle.get(position).getPrice());
                listBottle.set(position, resultBottle);
                Universal_Var_Cls.counter++;
                LiquorActivity.counterLayout.setVisibility(View.VISIBLE);

                System.out.println( " -----  count ------- " +  Universal_Var_Cls.counter);
                LiquorActivity.counterTxt.setText("" + Universal_Var_Cls.counter);


                Cursor cursor = database.query(CartHelper.TABLE_CART, null, null, null, null, null, null);

                if (cursor != null) {
                    boolean bool = false;
                    while (cursor.moveToNext()) {
                        if (cursor.getInt(0) == listBottle.get(position).getId()) {
                            bool = true;
                        }
                    }
                    if (bool) {
                        ContentValues values = new ContentValues();
                        values.put(CartHelper.KEY_ID, listBottle.get(position).getId()); // Contact Name
                        values.put(CartHelper.KEY_QTY, listBottle.get(position).getQuantity()); // Contact Phone
                        values.put(CartHelper.KEY_NAME, listBottle.get(position).getName());
                        values.put(CartHelper.KEY_CATEGORY, listBottle.get(position).getCategory());
                        values.put(CartHelper.KEY_IMAGE, listBottle.get(position).getImage());
                        values.put(CartHelper.KEY_PRICE, listBottle.get(position).getPrice());
                        values.put(CartHelper.KEY_QTY_LIQOUR, listBottle.get(position).getBottleMl());

                        // Inserting Row
                        database.update(CartHelper.TABLE_CART, values, CartHelper.KEY_ID + " = " + String.valueOf(listBottle.get(position).getId()), null);
                    } else {
                        ContentValues values = new ContentValues();
                        values.put(CartHelper.KEY_ID, listBottle.get(position).getId()); // Contact Name
                        values.put(CartHelper.KEY_QTY, listBottle.get(position).getQuantity()); // Contact Phone
                        values.put(CartHelper.KEY_NAME, listBottle.get(position).getName());
                        values.put(CartHelper.KEY_CATEGORY, listBottle.get(position).getCategory());
                        values.put(CartHelper.KEY_IMAGE, listBottle.get(position).getImage());
                        values.put(CartHelper.KEY_PRICE, listBottle.get(position).getPrice());
                        values.put(CartHelper.KEY_QTY_LIQOUR, listBottle.get(position).getBottleMl());

                        // Inserting Row
                        database.insert(CartHelper.TABLE_CART, null, values);
                    }
                }
                //              notifyItemChanged(position);
                //            Toast.makeText(context, ""+listBottle.get(position).getQuantity(), Toast.LENGTH_SHORT).show();
            }
        });
        holder.decBottleQtyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int qty = Integer.parseInt(holder.qtyBottle.getText().toString()) - 1;
                if (qty > 0) {
                    holder.qtyBottle.setText("" + (Integer.parseInt(holder.qtyBottle.getText().toString()) - 1));
                } else {
                    holder.addToCartBtn.setVisibility(View.VISIBLE);
                    holder.bottleIncDecLayout.setVisibility(View.GONE);
                    holder.qtyBottle.setText("0");
                }
                ResultBottle resultBottle = new ResultBottle();
                resultBottle.setQuantity(Integer.parseInt(holder.qtyBottle.getText().toString()));
                resultBottle.setBar(listBottle.get(position).getBar());
                resultBottle.setBottleMl(listBottle.get(position).getBottleMl());
                resultBottle.setCategory(listBottle.get(position).getCategory());
                resultBottle.setId(listBottle.get(position).getId());
                resultBottle.setImage(listBottle.get(position).getImage());
                resultBottle.setName(listBottle.get(position).getName());
                resultBottle.setPrice(listBottle.get(position).getPrice());
                listBottle.set(position, resultBottle);

                if (Universal_Var_Cls.counter >= 1) {
                    Universal_Var_Cls.counter--;
                }
                if (Universal_Var_Cls.counter == 0) {
                    LiquorActivity.counterLayout.setVisibility(View.GONE);
                } else {
                    LiquorActivity.counterLayout.setVisibility(View.VISIBLE);
                }

                LiquorActivity.counterTxt.setText("" + Universal_Var_Cls.counter);

                Cursor cursor = database.query(CartHelper.TABLE_CART, null, null, null, null, null, null);

                if (cursor != null) {
                    int pos = 0;
                    while (cursor.moveToNext()) {
                        if (cursor.getInt(0) == listBottle.get(position).getId()) {
                            ++pos;
                            if (qty == 0) {
                                database.delete(CartHelper.TABLE_CART, CartHelper.KEY_ID + "=" + cursor.getString(0), null);
                            }else {
                                ContentValues values = new ContentValues();
                                values.put(CartHelper.KEY_ID, listBottle.get(position).getId()); // Contact Name
                                values.put(CartHelper.KEY_QTY, listBottle.get(position).getQuantity()); // Contact Phone
                                values.put(CartHelper.KEY_NAME, listBottle.get(position).getName());
                                values.put(CartHelper.KEY_CATEGORY, listBottle.get(position).getCategory());
                                values.put(CartHelper.KEY_IMAGE, listBottle.get(position).getImage());
                                values.put(CartHelper.KEY_PRICE, listBottle.get(position).getPrice());
                                values.put(CartHelper.KEY_QTY_LIQOUR, listBottle.get(position).getBottleMl());

                                // Inserting Row
                                database.update(CartHelper.TABLE_CART, values, CartHelper.KEY_ID + " = " + String.valueOf(listBottle.get(position).getId()), null);
                            }
                            break;
                        }
                    }
                }
                //              notifyItemChanged(position);
                //            Toast.makeText(context, ""+listBottle.get(position).getQuantity(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void bottleDetail(Integer position) {

        if (new PermissionManager().hasPermissions(context, SplashActivity.RUNTIME_PERMISSIONS)) {

            Integer in = listBottle.get(position).getId();
            Universal_Var_Cls.bottleId = Integer.parseInt(String.valueOf(in));
            Universal_Var_Cls.bottlePrice = listBottle.get(position).getPrice();
            Universal_Var_Cls.name = listBottle.get(position).getName();
            Universal_Var_Cls.category = listBottle.get(position).getCategory();
            Universal_Var_Cls.image = listBottle.get(position).getImage();
            Universal_Var_Cls.liqourQty = listBottle.get(position).getBottleMl();
            if (listBottle.get(position).getQuantity() > 0) {
                Universal_Var_Cls.bottleQty = listBottle.get(position).getQuantity();
            } else {
                Universal_Var_Cls.bottleQty = 0;
            }
            context.startActivity(new Intent(context, BottleDetailActivity.class));
        } else {
            ActivityCompat
                    .requestPermissions(activity, SplashActivity.RUNTIME_PERMISSIONS, 1);
            bottleDetail(position);
        }
    }

    @Override
    public int getItemCount() {
        return listBottle.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        ImageView imgBottle;
        TextView bottleBrandName, liqourCategory, bottlePrice, availableInBars;
        Button addToCartBtn;
        ImageButton incBottleQtyBtn, decBottleQtyBtn;
        TextView qtyBottle;
        LinearLayout bottleIncDecLayout, addToCartLayout;

        public VwHolder(@NonNull View itemView) {
            super(itemView);

            imgBottle = (ImageView) itemView.findViewById(R.id.img_bottle);
            bottleBrandName = (TextView) itemView.findViewById(R.id.liqour_brand);
            liqourCategory = (TextView) itemView.findViewById(R.id.liqour_category);
            bottlePrice = (TextView) itemView.findViewById(R.id.bottle_price);
            availableInBars = (TextView) itemView.findViewById(R.id.available_in_bars);
            addToCartBtn = (Button) itemView.findViewById(R.id.bottle_add_to_cart);
            incBottleQtyBtn = (ImageButton) itemView.findViewById(R.id.bottle_inc_btn);
            decBottleQtyBtn = (ImageButton) itemView.findViewById(R.id.bottle_dec_btn);
            qtyBottle = (TextView) itemView.findViewById(R.id.bottle_qty);
            bottleIncDecLayout = (LinearLayout) itemView.findViewById(R.id.bottle_inc_dec_layout);
            addToCartLayout = (LinearLayout) itemView.findViewById(R.id.add_to_cart_layout);
        }
    }
}
