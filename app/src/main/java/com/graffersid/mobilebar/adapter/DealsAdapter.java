package com.graffersid.mobilebar.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.model_cls.my_bar.Result;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Sandy on 16/09/2019 at 12:55 PM.
 */
public class DealsAdapter extends RecyclerView.Adapter<DealsAdapter.VwHolder> {

    Context context;
    List<Result> listDeals;
    LayoutInflater inflater;

    public DealsAdapter(Context context, List<Result> listDeals) {
        this.context = context;
        this.listDeals = listDeals;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_deals_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, int position) {

        Picasso.with(context).load(listDeals.get(position).getImageUrl()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(holder.imageView);

        holder.textView.setText(listDeals.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return listDeals.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;
        public VwHolder(@NonNull View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.deals_img);
            textView = (TextView) itemView.findViewById(R.id.deals_txt);
        }
    }
}
