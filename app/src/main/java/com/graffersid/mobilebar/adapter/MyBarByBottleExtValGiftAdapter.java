package com.graffersid.mobilebar.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.my_bar.Datum;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static java.lang.Math.round;

/**
 * Created by Sandy on 17/09/2019 at 06:08 PM.
 */
public class MyBarByBottleExtValGiftAdapter extends RecyclerView.Adapter<MyBarByBottleExtValGiftAdapter.VwHolder> {

    Context context;
    List<Datum> listGiftExtVal;
    LayoutInflater inflater;

    Drawable wrappedDrawable;
    static LinearLayout layoutQtyBar, layoutColorBar;
    static RelativeLayout layoutQtyBack;
    static TextView txtQtyMargin, txtQty, txtColorMargin, txtColor;
    public static RadioButton radioButton;
    List<Boolean> listSelected;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public MyBarByBottleExtValGiftAdapter(Context context, List<Datum> listGiftExtVal, List<Boolean> listSelected) {

        this.context = context;
        this.listGiftExtVal = listGiftExtVal;
        this.listSelected = listSelected;

        inflater = LayoutInflater.from(context);

        preferences = context.getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(context, R.drawable.back_liquor_qty);
        wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_gift_by_bottle, parent, false));
    }


    RadioButton radioButtonSelected = null;
    int lastCheckedPosition = -1;
    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, final int position) {

        if (listGiftExtVal.get(position).getBottleImage() != null){

            Picasso.with(context).load(Universal_Var_Cls.baseUrl+listGiftExtVal.get(position).getBottleImage()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(holder.img_gift_bottle);
        }

        holder.brandNameGiftDialog.setText(listGiftExtVal.get(position).getBottleName());
        holder.expDateBottleGiftDialog.setText("Till: "+listGiftExtVal.get(position).getExipry());

        float remain;
        float sum;
        Float totalMl = listGiftExtVal.get(position).getTotalMl();
        Float remainingMl = listGiftExtVal.get(position).getRemainingMl();

        if (totalMl != null) {
            if (remainingMl != null && totalMl >= remainingMl) {

                sum = Float.parseFloat(String.format("%.2f", totalMl));
                remain = Float.parseFloat(String.format("%.2f", remainingMl));

                /*BigDecimal bd;

                bd = new BigDecimal(totalMl.floatValue());
                bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
                sum = bd.floatValue();

                bd = new BigDecimal(remainingMl.floatValue());
                bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
                remain = bd.floatValue();*/

                layoutQtyBar.setWeightSum(sum);
                layoutColorBar.setWeightSum(sum);
                setColorMethod(remain, sum);
            }
        }

        if (listSelected.get(position)){
            radioButton.setChecked(listSelected.get(position));

            radioButtonSelected = radioButton;
            lastCheckedPosition = position;
        }

        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (radioButtonSelected != null){
                    if (radioButtonSelected == (RadioButton) view) {

                    } else {
                        radioButtonSelected.setChecked(false);
                    }
                    listSelected.set(lastCheckedPosition, !listSelected.get(lastCheckedPosition));
                }
                radioButtonSelected = (RadioButton) view;
                listSelected.set(position, !listSelected.get(position));
                lastCheckedPosition = position;

                int purchasedId = listGiftExtVal.get(position).getId();
                editor.putInt(Universal_Var_Cls.bottlePurchasedId, purchasedId);
  //              editor.putFloat(Universal_Var_Cls.priceExtend, listGiftExtVal.get(position).getPrice());
                editor.commit();
                Universal_Var_Cls.bottlePrice = listGiftExtVal.get(position).getPrice();
                Universal_Var_Cls.bottleIdPurchased = purchasedId;

                Log.d("json_ext_val", ""+listGiftExtVal.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listGiftExtVal.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        TextView brandNameGiftDialog, expDateBottleGiftDialog;
        ImageView img_gift_bottle;

        public VwHolder(@NonNull View itemView) {
            super(itemView);

            layoutQtyBar = (LinearLayout) itemView.findViewById(R.id.gift_liqour_qty_bar_layout);
            layoutQtyBack = (RelativeLayout) itemView.findViewById(R.id.gift_liqour_qty_back);
            txtQtyMargin = (TextView) itemView.findViewById(R.id.gift_liqour_qty_margin);
            txtQty = (TextView) itemView.findViewById(R.id.gift_liqour_qty);

            layoutColorBar = (LinearLayout) itemView.findViewById(R.id.gift_liqour_bar_layout);
            txtColorMargin = (TextView) itemView.findViewById(R.id.gift_liqour_margin_color_bar);
            txtColor = (TextView) itemView.findViewById(R.id.gift_liqour_color_bar);


            brandNameGiftDialog = (TextView) itemView.findViewById(R.id.brand_name_gift_dialog);
            expDateBottleGiftDialog = (TextView) itemView.findViewById(R.id.exp_date_bottle_gift_dialog);
            img_gift_bottle = (ImageView) itemView.findViewById(R.id.img_gift_bottle);


            radioButton = (RadioButton) itemView.findViewById(R.id.gift_radio_btn);

            if (Universal_Var_Cls.isExtGift){
                radioButton.setVisibility(View.GONE);
            }else {
                radioButton.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setColorMethod(float remain, float sum) {

        float marginBar = sum - remain;
        float limitBar = sum/100;   // 1% of total liquor

        if (remain >= (limitBar*75)){

            DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.liqour_green));
            layoutQtyBack.setBackgroundResource(0);
            layoutQtyBack.setBackgroundResource(R.drawable.back_liquor_qty);
            txtQty.setTextColor(context.getResources().getColor(R.color.liqour_green_txt));
            txtQty.setText(""+(int)remain+"ml left");

            txtQtyMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColorMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColor.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, remain));

            if (marginBar == 0){
                txtQtyMargin.setVisibility(View.GONE);
                txtColorMargin.setVisibility(View.GONE);
            }else {
                txtQtyMargin.setVisibility(View.VISIBLE);
                txtColorMargin.setVisibility(View.VISIBLE);
            }
        }else if (remain >= (limitBar*35)){
            DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.liqour_yellow));
            layoutQtyBack.setBackgroundResource(0);
            layoutQtyBack.setBackgroundResource(R.drawable.back_liquor_qty);
            txtQty.setTextColor(context.getResources().getColor(R.color.liqour_yellow_txt));
            txtQty.setText(""+(int)remain+"ml left");

            txtQtyMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));

            txtColorMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColor.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, remain));
        }else if (remain < (limitBar*35)){

            DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.liqour_red));
            layoutQtyBack.setBackgroundResource(0);
            layoutQtyBack.setBackgroundResource(R.drawable.back_liquor_qty);
            txtQty.setTextColor(context.getResources().getColor(R.color.liqour_red_txt));
            txtQty.setText(""+(int)remain+"ml left");

            txtQtyMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));

            txtColorMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColor.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, remain));
        }
    }
}
