package com.graffersid.mobilebar.frags;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.CartActivity;
import com.graffersid.mobilebar.activities.ExtendValidityActivity;
import com.graffersid.mobilebar.activities.GiftActivity;
import com.graffersid.mobilebar.adapter.BottleConsumeHistoryAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.bottle_history.BottleHistory;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class BottleConsumeHistoryFragment extends Fragment {

    View view, vwBottleBottom;
    RecyclerView recyclerView;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    HashMap<String, String> headers;
    BottleConsumeHistoryAdapter adapter;
    Context context;
    LinearLayout layoutLeft, layoutRight;
    Button btnExtVal, btnGiftFrnd, btnRenewBottle;
    ImageView imgBottle;
    TextView nameBottle, nameBrand, statusRemainOrExp, statusRemainMl, datePurchased, dateExp;
    private static int ml;
    private TextView noHistory;

    public BottleConsumeHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_bottle_consume_history, container, false);


        context = getActivity();

        Universal_Var_Cls.loader(context);

        noHistory = (TextView) view.findViewById(R.id.no_history);

        noHistory.setVisibility(View.GONE);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_consume_bottle);
        layoutLeft = (LinearLayout) view.findViewById(R.id.layout_left);
        layoutRight = (LinearLayout) view.findViewById(R.id.layout_right);

        btnExtVal = (Button) view.findViewById(R.id.extend_validity_btn);
        btnGiftFrnd = (Button) view.findViewById(R.id.gift_a_friend);

        imgBottle = (ImageView) view.findViewById(R.id.img_bottle_history);
        vwBottleBottom = (View) view.findViewById(R.id.view_bottle_bottom);

        nameBottle = (TextView) view.findViewById(R.id.bottle_name);
        nameBrand = (TextView) view.findViewById(R.id.brand_name);
        statusRemainOrExp = (TextView) view.findViewById(R.id.status_remain_exp);
        statusRemainMl = (TextView) view.findViewById(R.id.status_remain_exp_ml);
        btnRenewBottle = (Button) view.findViewById(R.id.btn_renew_bottle);
        datePurchased = (TextView) view.findViewById(R.id.date_purchased);
        dateExp = (TextView) view.findViewById(R.id.date_exp);

        layoutLeft.setVisibility(View.INVISIBLE);
        layoutRight.setVisibility(View.INVISIBLE);

        recyclerView.setPadding(0, 10, 0, 40);

        preferences = getActivity().getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        ml = 0;

        historyList();


        btnExtVal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ml > 0){
                    editor.putInt(Universal_Var_Cls.bottlePurchasedId,  Universal_Var_Cls.bottleIdPurchased);
                    editor.commit();

                    context.startActivity(new Intent(context, ExtendValidityActivity.class));
                }else {
                    Toast.makeText(context, "You have insufficient amount of liquor. Please renew the bottle", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnGiftFrnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ml > 0){
                    editor.putInt(Universal_Var_Cls.bottlePurchasedId, Universal_Var_Cls.bottleIdPurchased);
                    editor.commit();
                    context.startActivity(new Intent(context, GiftActivity.class));
                }else {
                    Toast.makeText(context, "You have insufficient amount of liquor. Please renew the bottle", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnRenewBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Universal_Var_Cls.isRenew = true;
                context.startActivity(new Intent(context, CartActivity.class));
            }
        });
        return view;
    }


    private void historyList() {

        Universal_Var_Cls.dialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Universal_Var_Cls.bottleConsumptionHistory+Universal_Var_Cls.bottleIdPurchased, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("bottle_consume_h", response.toString());

                Universal_Var_Cls.dialog.dismiss();

                BottleHistory bottleHistory = new Gson().fromJson(response.toString(), BottleHistory.class);

                Universal_Var_Cls.bottlePrice = bottleHistory.getData().getPrice();

                if (bottleHistory.getData().getOrderdrink() != null && !bottleHistory.getData().getOrderdrink().isEmpty()) {

                    adapter = new BottleConsumeHistoryAdapter(getActivity(), bottleHistory.getData().getOrderdrink());

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setAdapter(adapter);
                    noHistory.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }else {
                    noHistory.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }

                ml = bottleHistory.getData().getRemainingMl();

                Universal_Var_Cls.bottleId = bottleHistory.getData().getBottleId();
                Universal_Var_Cls.nameBottleRenew = bottleHistory.getData().getBottleName();
                Universal_Var_Cls.imgBottleRenew = bottleHistory.getData().getBottleImage();
                Universal_Var_Cls.brandNameBottleRenew = bottleHistory.getData().getCategory();
                Universal_Var_Cls.priceBottleRenew = bottleHistory.getData().getPrice();
                Universal_Var_Cls.qtyLiqourBottleRenew = 750;

                btnRenewBottle.setVisibility(View.VISIBLE);


                if (bottleHistory.getData().getBottleImage() != null){
                    Picasso.with(context).load(Universal_Var_Cls.baseUrl+bottleHistory.getData().getBottleImage()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(imgBottle);
                }
                if (bottleHistory.getData().getBottleName() != null){
                    nameBottle.setText(bottleHistory.getData().getBottleName());
                }
                if (bottleHistory.getData().getCategory() != null){
                    nameBrand.setText(bottleHistory.getData().getCategory());
                }

                if (bottleHistory.getData().getRemainingMl() > 0){
                    statusRemainMl.setText(""+bottleHistory.getData().getRemainingMl()+"ml left in bottle");
                }else {
                    statusRemainMl.setText("0ml left in bottle");
                }

                try {
                    String exp = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yy/MMMM/dd").parse(bottleHistory.getData().getExpiry()));
                    String toDay = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
                    String exp22 = new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yy/MMMM/dd").parse(bottleHistory.getData().getExpiry()));

                    if (toDay.compareTo(exp) > 0){
                        statusRemainOrExp.setText("Expired");
                        statusRemainOrExp.setTextColor(getResources().getColor(R.color.bottle_price));
                        vwBottleBottom.setBackgroundColor(getResources().getColor(R.color.bottle_price));

                        dateExp.setText("Expired: "+exp22);
                    }else {

                        statusRemainOrExp.setText("Remaining");
                        statusRemainOrExp.setTextColor(getResources().getColor(R.color.liqour_green_txt));
                        vwBottleBottom.setBackgroundColor(getResources().getColor(R.color.liqour_green_txt));
                        layoutLeft.setVisibility(View.VISIBLE);
                        layoutRight.setVisibility(View.VISIBLE);

                        dateExp.setText("Till: "+exp22);
                    }


                    String purchase = new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yy/MMMM/dd").parse(bottleHistory.getData().getBottlePurchasedDate()));
                    datePurchased.setText("Purchased: "+purchase);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                Log.d("bottle_consume_he", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
