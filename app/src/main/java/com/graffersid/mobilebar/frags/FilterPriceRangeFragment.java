package com.graffersid.mobilebar.frags;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterPriceRangeFragment extends Fragment {

    TextView  minimumPrice, maximumPrice;
   public static EditText minPrice, maxPrice;

    public FilterPriceRangeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter_price_range, container, false);

        minPrice = view.findViewById(R.id.min_price_filter);
        maxPrice = view.findViewById(R.id.max_price_filter);

        minimumPrice = (TextView) view.findViewById(R.id.minimum_price_filter);
        maximumPrice = (TextView) view.findViewById(R.id.maximum_price_filter);
        if (Universal_Var_Cls.minSelectedFilterPrice >= 0){
            minPrice.setText(String.valueOf(Universal_Var_Cls.minSelectedFilterPrice));
        }
        if (Universal_Var_Cls.maxSelectedFilterPrice >= 0){
            maxPrice.setText(String.valueOf(Universal_Var_Cls.maxSelectedFilterPrice));
        }

        minPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s!=null && s.length()>0)
                Universal_Var_Cls.minSelectedFilterPrice = Integer.parseInt(String.valueOf(s));
                else
                    Universal_Var_Cls.minSelectedFilterPrice=0;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        maxPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s!=null && s.length()>0)
                    Universal_Var_Cls.maxSelectedFilterPrice = Integer.parseInt(String.valueOf(s));
                else
                Universal_Var_Cls.maxSelectedFilterPrice=0;

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

}
