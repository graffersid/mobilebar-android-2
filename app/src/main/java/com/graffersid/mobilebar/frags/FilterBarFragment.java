package com.graffersid.mobilebar.frags;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.FilterBarAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.filter_category_list.FilterCategoryList;
import com.graffersid.mobilebar.model_cls.filter_category_list.ResultCategory;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterBarFragment extends Fragment {

    RecyclerView recyclerView;
    Context context;
    public static FilterBarAdapter adapter;
    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    String nextPage, previousPage;

    public FilterBarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter_bar, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.bar_filter_recycler_vw);
        context = getActivity();

        if (Universal_Var_Cls.filterBarList.getCount() == 0) {

        } else {
            prepareBarList();
        }
        return view;
    }

    private void prepareBarList() {

        Universal_Var_Cls.listBar = new ArrayList<>();

        layoutManager = new GridLayoutManager(context, 1);
        adapter = new FilterBarAdapter(context, Universal_Var_Cls.listBar);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        ResultCategory resultCategory;
        for (int i = 0; i < Universal_Var_Cls.filterBarList.getResults().size(); i++) {
            resultCategory = new ResultCategory();
            resultCategory.setId(Universal_Var_Cls.filterBarList.getResults().get(i).getId());
            resultCategory.setImage(Universal_Var_Cls.filterBarList.getResults().get(i).getImage());
            resultCategory.setName(Universal_Var_Cls.filterBarList.getResults().get(i).getName());
            resultCategory.setAddress(Universal_Var_Cls.filterBarList.getResults().get(i).getAddress());

            if (Universal_Var_Cls.filterBarIdList.size() > i){
                if (Universal_Var_Cls.filterBarIdList.get(i).equals("-1")){
                    resultCategory.setIsSelect(false);
                }else {
                    resultCategory.setIsSelect(true);
                }
            }else {
                resultCategory.setIsSelect(false);
                Universal_Var_Cls.filterBarIdList.add("-1");
            }
            Universal_Var_Cls.listBar.add(resultCategory);
            adapter.notifyDataSetChanged();
        }

        nextPage = Universal_Var_Cls.filterBarList.getNext();
        if (nextPage != null){

            String str = nextPage;

            if (str.contains("localhost")){
                str = str.replace("localhost", Universal_Var_Cls.baseDomain);
            }
            nextPage = str;

            categoryListData(nextPage);
        }
        paginationMethod();
    }

    private void paginationMethod() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isPaginate = true;
                                categoryListData(nextPage);
                            }
                        }, 100);
                    }
                }
            }
        });
    }

    private void categoryListData(final String url) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                FilterCategoryList filterBarList = new Gson().fromJson(response.toString(), FilterCategoryList.class);

                if (filterBarList.getNext() != null){

                    String str = filterBarList.getNext();

                    if (str.contains("localhost")){
                        str = str.replace("localhost", Universal_Var_Cls.baseDomain);
                    }
                    nextPage = str;
                }else {
                    nextPage = null;
                }

                ResultCategory resultCategory;
                for (int i = 0; i < filterBarList.getResults().size(); i++) {

                    resultCategory = new ResultCategory();
                    resultCategory.setId(filterBarList.getResults().get(i).getId());
                    resultCategory.setName(filterBarList.getResults().get(i).getName());
                    resultCategory.setImage(filterBarList.getResults().get(i).getImage());

                    boolean idCat = false;
                    int k;
                    for (k=0; k<Universal_Var_Cls.filterBarIdList.size(); k++){
                        if (String.valueOf(filterBarList.getResults().get(i).getId()).equals(Universal_Var_Cls.filterBarIdList.get(k))){
                            idCat = true;
                            break;
                        }
                    }
                    if (idCat){
                        resultCategory.setIsSelect(true);
                    }else {
                        resultCategory.setIsSelect(false);
                        if (Universal_Var_Cls.filterBarIdList.size() <= i){
                            Universal_Var_Cls.filterBarIdList.add("-1");
                        }
                    }
                    Universal_Var_Cls.listBar.add(resultCategory);
                    adapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        });

        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
