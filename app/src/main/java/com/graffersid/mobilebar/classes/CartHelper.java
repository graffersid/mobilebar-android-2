package com.graffersid.mobilebar.classes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Sandy on 09/08/2019 at 02:11 PM.
 */
public class CartHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "cartLiqour";
    public static final String TABLE_CART = "cartDetail";
    public static String KEY_ID = "id";
    public static final String KEY_QTY = "qty";
    public static final String KEY_IMAGE = "image", KEY_NAME = "name", KEY_CATEGORY = "category";
    public static final String KEY_QTY_LIQOUR = "qtyLiqour";
    public static final String KEY_PRICE = "price";

    public CartHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_CART+"("+
                KEY_ID+" INTEGER PRIMARY KEY, "+
                KEY_QTY+" INTEGER, "+
                KEY_NAME+" text, "+
                KEY_CATEGORY+" text, "+
                KEY_IMAGE+" text, "+
                KEY_QTY_LIQOUR+" INTEGER, "+
                KEY_PRICE+" REAL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
