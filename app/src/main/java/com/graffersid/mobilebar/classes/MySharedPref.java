package com.graffersid.mobilebar.classes;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPref {
    private static final String SHARED_PREFS_NAME = "preferences";

    static SharedPreferences sp;
    private static MySharedPref instance;

    private SharedPreferences sharedPreferences;

    private MySharedPref() {
        instance = this;

        sharedPreferences = FabricCrashlytics.getInstance().getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized MySharedPref getInstance() {
        if (instance == null) {
            instance = new MySharedPref();
        }

        return instance;
    }

    public static void saveData(Context context, String key, String value)
    {
        sp = context.getSharedPreferences(SHARED_PREFS_NAME,context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public static void saveData(Context context, String key, boolean value)
    {
        sp = context.getSharedPreferences(SHARED_PREFS_NAME,context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(key,value);
        editor.commit();
    }
    public static String getData(Context context, String key, String value)
    {
        sp = context.getSharedPreferences(SHARED_PREFS_NAME,context.MODE_PRIVATE);
        return sp.getString(key,value);
    }
    public static boolean getData(Context context, String key, boolean value)
    {
        sp = context.getSharedPreferences(SHARED_PREFS_NAME,context.MODE_PRIVATE);
        return sp.getBoolean(key,value);
    }

    public static void DeleteData(Context context)
    {
        sp = context.getSharedPreferences(SHARED_PREFS_NAME,context.MODE_PRIVATE);
        sp.edit().clear().commit();
    }

    public static void NullData(Context context ,String key)
    {
        sp = context.getSharedPreferences(SHARED_PREFS_NAME,context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key,null);
        editor.commit();
    }




    public void delete(String key) {
        if (sharedPreferences.contains(key)) {
            getEditor().remove(key).commit();
        }
    }

    public static void save(String key, Object value) {
        SharedPreferences.Editor editor = sp.edit();
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Enum) {
            editor.putString(key, value.toString());
        } else if (value != null) {
            throw new RuntimeException("Attempting to save non-supported preference");
        }

        editor.commit();
    }


    @SuppressWarnings("unchecked")
    public <T> T get(String key, T defValue) {
        T returnValue = (T) sharedPreferences.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }
}
