package com.graffersid.mobilebar.classes;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by Sandy Pati on 20-10-2019 at 08:33 PM.
 */
public class VolleyErrorCls {

    public static void volleyErr(VolleyError error, Context context) {

        if (error instanceof NetworkError) {

            if (error.networkResponse != null) {
                //               Log.d("resp_bottle_save_e", String.valueOf(error.networkResponse.statusCode));
                try {
                    String responseBody = new String(error.networkResponse.data, "utf-8");
                    JSONObject data = new JSONObject(responseBody);
            /*JSONArray errors = data.getJSONArray("errors");
            JSONObject jsonMessage = errors.getJSONObject(0);*/
                    String message = data.getString("message");
                    message += "    "+data.getString("data");
                    //                   message = data.toString();


                    //                 Log.d("resp_bottle_save_e", message);
                    Log.d("resp_bottle_save_e", data.toString());
                    Toast.makeText(context, data.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                } catch (UnsupportedEncodingException errorr) {
                }
            } else {
//            Toast.makeText(this.context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
                Log.d("resp_bottle_save_e", error.toString());
            }
        }else if (error instanceof ServerError){

            if (error.networkResponse != null) {
                //              Log.d("resp_bottle_save_e", String.valueOf(error.networkResponse.statusCode));
                try {
                    String responseBody = new String(error.networkResponse.data, "utf-8");
                    JSONObject data = new JSONObject(responseBody);
            /*JSONArray errors = data.getJSONArray("errors");
            JSONObject jsonMessage = errors.getJSONObject(0);*/
                    String message = data.getString("message");
                    message += "    "+ data.getString("data");
                    //                 message = data.toString();


                    //                 Log.d("resp_bottle_save_e", message);
                    Log.d("resp_bottle_save_e", data.toString());
                    Toast.makeText(context, data.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                } catch (UnsupportedEncodingException errorr) {
                }
            } else {
//            Toast.makeText(this.context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
                Log.d("resp_bottle_save_e", error.toString());
            }
        }else {
            Log.d("resp_bottle_save_e", error.toString());
        }
    }
}
