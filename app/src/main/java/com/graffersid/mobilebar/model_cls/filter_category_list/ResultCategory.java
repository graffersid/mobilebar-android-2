package com.graffersid.mobilebar.model_cls.filter_category_list;

/**
 * Created by Sandy on 30/07/2019 at 04:58 PM.
 */
public class ResultCategory {

    private Long id;
    private String image;
    private String name;
    private Boolean isSelect;
    private String address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(Boolean select) {
        isSelect = select;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
