package com.graffersid.mobilebar.model_cls.consume_history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 01/10/2019 at 07:21 PM.
 */
public class Datum {

    @SerializedName("bar_name")
    @Expose
    private String barName;
    @SerializedName("bottle_name")
    @Expose
    private String bottleName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("consumed_ml")
    @Expose
    private Integer consumedMl;
    @SerializedName("chargable_ml")
    @Expose
    private Integer chargableMl;

    public String getBarName() {
        return barName;
    }

    public void setBarName(String barName) {
        this.barName = barName;
    }

    public String getBottleName() {
        return bottleName;
    }

    public void setBottleName(String bottleName) {
        this.bottleName = bottleName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getConsumedMl() {
        return consumedMl;
    }

    public void setConsumedMl(Integer consumedMl) {
        this.consumedMl = consumedMl;
    }

    public Integer getChargableMl() {
        return chargableMl;
    }

    public void setChargableMl(Integer chargableMl) {
        this.chargableMl = chargableMl;
    }

}
