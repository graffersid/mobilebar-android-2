package com.graffersid.mobilebar.model_cls.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 07/10/2019 at 11:32 AM.
 */
public class Reffer {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("point")
        @Expose
        private Integer point;
        @SerializedName("name")
        @Expose
        private String name;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getPoint() {
            return point;
        }

        public void setPoint(Integer point) {
            this.point = point;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
