package com.graffersid.mobilebar.model_cls.consume;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 27/09/2019 at 11:17 AM.
 */
public class ConversionMl {

    @SerializedName("bar")
    @Expose
    private Bar bar;
    @SerializedName("bottle")
    @Expose
    private Bottle bottle;
    @SerializedName("barbottle")
    @Expose
    private Barbottle barbottle;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public Bar getBar() {
        return bar;
    }

    public void setBar(Bar bar) {
        this.bar = bar;
    }

    public Bottle getBottle() {
        return bottle;
    }

    public void setBottle(Bottle bottle) {
        this.bottle = bottle;
    }

    public Barbottle getBarbottle() {
        return barbottle;
    }

    public void setBarbottle(Barbottle barbottle) {
        this.barbottle = barbottle;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}