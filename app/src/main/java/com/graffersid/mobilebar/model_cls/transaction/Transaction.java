package com.graffersid.mobilebar.model_cls.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 07/10/2019 at 11:30 AM.
 */
public class Transaction {

    @SerializedName("purchased")
    @Expose
    private List<Purchased> purchased = null;
    @SerializedName("extend")
    @Expose
    private List<Extend> extend = null;
    @SerializedName("reffer")
    @Expose
    private List<Reffer> reffer = null;

    public List<Purchased> getPurchased() {
        return purchased;
    }

    public void setPurchased(List<Purchased> purchased) {
        this.purchased = purchased;
    }

    public List<Extend> getExtend() {
        return extend;
    }

    public void setExtend(List<Extend> extend) {
        this.extend = extend;
    }

    public List<Reffer> getReffer() {
        return reffer;
    }

    public void setReffer(List<Reffer> reffer) {
        this.reffer = reffer;
    }

}
