package com.graffersid.mobilebar.model_cls;

/**
 * Created by Sandy on 08/08/2019 at 04:56 PM.
 */
public class CartBottle {

    String image, name, category;
    int qtyBottle, qtyLiqour, id;
    double price;

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public int getQtyBottle() {
        return qtyBottle;
    }
    public void setQtyBottle(int qtyBottle) {
        this.qtyBottle = qtyBottle;
    }
    public int getQtyLiqour() {
        return qtyLiqour;
    }
    public void setQtyLiqour(int qtyLiqour) {
        this.qtyLiqour = qtyLiqour;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
}
