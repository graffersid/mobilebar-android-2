package com.graffersid.mobilebar.model_cls.consume;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 27/09/2019 at 11:19 AM.
 */
public class Barbottle {

    @SerializedName("conversion_ml")
    @Expose
    private Double  conversionMl;

    public Double  getConversionMl() {
        return conversionMl;
    }

    public void setConversionMl(Double  conversionMl) {
        this.conversionMl = conversionMl;
    }

}
