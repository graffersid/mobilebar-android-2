package com.graffersid.mobilebar.model_cls.bottle_history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
* Created by Sandy on 03/10/2019 at 03:20 PM.
*/
public class Data {

    @SerializedName("bottle_id")
    @Expose
    private Integer bottleId;
    @SerializedName("bottle_name")
    @Expose
    private String bottleName;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("bottle_image")
    @Expose
    private String bottleImage;
    @SerializedName("remaining_ml")
    @Expose
    private Integer remainingMl;
    @SerializedName("expiry")
    @Expose
    private String expiry;
    @SerializedName("bottle_purchased_date")
    @Expose
    private String bottlePurchasedDate;
    @SerializedName("orderdrink")
    @Expose
    private List<Orderdrink> orderdrink = null;
    @SerializedName("price")
    @Expose
    private Double price;

    public Integer getBottleId() {
        return bottleId;
    }

    public void setBottleId(Integer bottleId) {
        this.bottleId = bottleId;
    }

    public String getBottleName() {
        return bottleName;
    }

    public void setBottleName(String bottleName) {
        this.bottleName = bottleName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBottleImage() {
        return bottleImage;
    }

    public void setBottleImage(String bottleImage) {
        this.bottleImage = bottleImage;
    }

    public Integer getRemainingMl() {
        return remainingMl;
    }

    public void setRemainingMl(Integer remainingMl) {
        this.remainingMl = remainingMl;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getBottlePurchasedDate() {
        return bottlePurchasedDate;
    }

    public void setBottlePurchasedDate(String bottlePurchasedDate) {
        this.bottlePurchasedDate = bottlePurchasedDate;
    }

    public List<Orderdrink> getOrderdrink() {
        return orderdrink;
    }

    public void setOrderdrink(List<Orderdrink> orderdrink) {
        this.orderdrink = orderdrink;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
