package com.graffersid.mobilebar.model_cls.gift;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 20/09/2019 at 05:37 PM.
 */
public class Result {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("bottle_name")
    @Expose
    private String bottleName;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("customer_by_id")
    @Expose
    private String customerById;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("is_accepted")
    @Expose
    private Boolean isAccepted;
    @SerializedName("date")
    @Expose
    private String date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBottleName() {
        return bottleName;
    }

    public void setBottleName(String bottleName) {
        this.bottleName = bottleName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCustomerById() {
        return customerById;
    }

    public void setCustomerById(String customerById) {
        this.customerById = customerById;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
