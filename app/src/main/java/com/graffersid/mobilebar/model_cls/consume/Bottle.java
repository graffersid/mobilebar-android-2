package com.graffersid.mobilebar.model_cls.consume;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 27/09/2019 at 11:19 AM.
 */
public class Bottle {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("remaining_ml")
    @Expose
    private Double remainingMl;
    @SerializedName("total_ml")
    @Expose
    private Double totalMl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getRemainingMl() {
        return remainingMl;
    }

    public void setRemainingMl(Double remainingMl) {
        this.remainingMl = remainingMl;
    }

    public Double getTotalMl() {
        return totalMl;
    }

    public void setTotalMl(Double totalMl) {
        this.totalMl = totalMl;
    }

}
