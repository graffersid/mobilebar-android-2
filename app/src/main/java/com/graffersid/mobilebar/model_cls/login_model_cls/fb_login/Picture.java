package com.graffersid.mobilebar.model_cls.login_model_cls.fb_login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 22/08/2019 at 04:03 PM.
 */
public class Picture {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
