package com.graffersid.mobilebar.model_cls.btl_detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 02/08/2019 at 03:39 PM.
 */
public class Bar {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("xcord")
    @Expose
    private Double xcord;
    @SerializedName("ycord")
    @Expose
    private Double ycord;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("adress")
    @Expose
    private String adress;
    @SerializedName("conversion_ML")
    @Expose
    private Double conversionML;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("distance")
    @Expose
    private Double distance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getXcord() {
        return xcord;
    }

    public void setXcord(Double xcord) {
        this.xcord = xcord;
    }

    public Double getYcord() {
        return ycord;
    }

    public void setYcord(Double ycord) {
        this.ycord = ycord;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Double getConversionML() {
        return conversionML;
    }

    public void setConversionML(Double conversionML) {
        this.conversionML = conversionML;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

}
