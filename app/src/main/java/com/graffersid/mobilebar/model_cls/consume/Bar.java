package com.graffersid.mobilebar.model_cls.consume;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 27/09/2019 at 11:19 AM.
 */
public class Bar {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("bar_pin")
    @Expose
    private String barPin;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBarPin() {
        return barPin;
    }

    public void setBarPin(String barPin) {
        this.barPin = barPin;
    }

}
