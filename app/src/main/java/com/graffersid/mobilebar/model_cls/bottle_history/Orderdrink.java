package com.graffersid.mobilebar.model_cls.bottle_history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
* Created by Sandy on 03/10/2019 at 03:20 PM.
*/
public class Orderdrink {

    @SerializedName("bar")
    @Expose
    private String bar;
    @SerializedName("orderdrinkdate")
    @Expose
    private String orderdrinkdate;
    @SerializedName("consumed_ml")
    @Expose
    private Integer consumedMl;
    @SerializedName("chargable_ml")
    @Expose
    private Integer chargableMl;

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }

    public String getOrderdrinkdate() {
        return orderdrinkdate;
    }

    public void setOrderdrinkdate(String orderdrinkdate) {
        this.orderdrinkdate = orderdrinkdate;
    }

    public Integer getConsumedMl() {
        return consumedMl;
    }

    public void setConsumedMl(Integer consumedMl) {
        this.consumedMl = consumedMl;
    }

    public Integer getChargableMl() {
        return chargableMl;
    }

    public void setChargableMl(Integer chargableMl) {
        this.chargableMl = chargableMl;
    }

}
