package com.graffersid.mobilebar.model_cls.bars;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 04/10/2019 at 03:01 PM.
 */
public class Bar {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("xcord")
    @Expose
    private Double xcord;
    @SerializedName("ycord")
    @Expose
    private Double ycord;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("conversion_ml")
    @Expose
    private Double conversionMl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getXcord() {
        return xcord;
    }

    public void setXcord(Double xcord) {
        this.xcord = xcord;
    }

    public Double getYcord() {
        return ycord;
    }

    public void setYcord(Double ycord) {
        this.ycord = ycord;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getConversionMl() {
        return conversionMl;
    }

    public void setConversionMl(Double conversionMl) {
        this.conversionMl = conversionMl;
    }

}
