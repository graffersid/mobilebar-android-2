
package com.graffersid.mobilebar.model_cls.liquor_list;

import java.util.List;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class LiquorBottleListCls {

    @Expose
    private Integer count;
    @Expose
    private String next;
    @Expose
    private Object previous;
    @Expose
    private List<Result> results;
    @Expose
    private Boolean status;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
