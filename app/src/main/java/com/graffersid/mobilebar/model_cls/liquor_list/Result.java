
package com.graffersid.mobilebar.model_cls.liquor_list;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Result {

    @Expose
    private List<Integer> bar;
    @SerializedName("bottle_ml")
    private Double bottleMl;
    @Expose
    private String category;
    @Expose
    private Integer id;
    @Expose
    private String image;
    @Expose
    private String name;
    @Expose
    private Double price;

    public List<Integer> getBar() {
        return bar;
    }

    public void setBar(List<Integer> bar) {
        this.bar = bar;
    }

    public Double getBottleMl() {
        return bottleMl;
    }

    public void setBottleMl(Double bottleMl) {
        this.bottleMl = bottleMl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
