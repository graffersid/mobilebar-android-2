package com.graffersid.mobilebar.model_cls.liquor_list;

import java.util.List;

/**
 * Created by Sandy on 30/07/2019 at 10:31 AM.
 */
public class ResultBottle {

    private List<Integer> bar;
    private Double bottleMl;
    private String category;
    private Integer id;
    private String image;
    private String name;
    private Double price;
    private Integer quantity;

    public List<Integer> getBar() {
        return bar;
    }

    public void setBar(List<Integer> bar) {
        this.bar = bar;
    }

    public Double getBottleMl() {
        return bottleMl;
    }

    public void setBottleMl(Double bottleMl) {
        this.bottleMl = bottleMl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
