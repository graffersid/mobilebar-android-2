package com.graffersid.mobilebar.model_cls.my_bar;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 12/09/2019 at 03:52 PM.
 */
public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private Float price;
    @SerializedName("days")
    @Expose
    private Integer days;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("enddate")
    @Expose
    private String enddate;
    @SerializedName("startdate")
    @Expose
    private String startdate;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("is_deleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("bottle_id")
    @Expose
    private Integer bottleId;
    @SerializedName("bottle_name")
    @Expose
    private String bottleName;
    @SerializedName("bottle_image")
    @Expose
    private String bottleImage;
    @SerializedName("bottle_category")
    @Expose
    private String bottleCategory;
    @SerializedName("exipry")
    @Expose
    private String exipry;
    @SerializedName("remaining_ml")
    @Expose
    private Float remainingMl;
    @SerializedName("total_ml")
    @Expose
    private Float totalMl;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("bottle_detail")
    @Expose
    private String bottleDetail;
    @SerializedName("bottle_ml")
    @Expose
    private Integer bottleMl;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("expiry")
    @Expose
    private String expiry;
    @SerializedName("purchased_date")
    @Expose
    private String purchasedDate;
    @SerializedName("gift_date")
    @Expose
    private String giftDate;
    @SerializedName("is_accepted")
    @Expose
    private Boolean isAccepted;
    @SerializedName("gifted_ml")
    @Expose
    private Integer giftedMl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Integer getBottleId() {
        return bottleId;
    }

    public void setBottleId(Integer bottleId) {
        this.bottleId = bottleId;
    }

    public String getBottleName() {
        return bottleName;
    }

    public void setBottleName(String bottleName) {
        this.bottleName = bottleName;
    }

    public String getBottleImage() {
        return bottleImage;
    }

    public void setBottleImage(String bottleImage) {
        this.bottleImage = bottleImage;
    }

    public String getBottleCategory() {
        return bottleCategory;
    }

    public void setBottleCategory(String bottleCategory) {
        this.bottleCategory = bottleCategory;
    }

    public String getExipry() {
        return exipry;
    }

    public void setExipry(String exipry) {
        this.exipry = exipry;
    }

    public Float getRemainingMl() {
        return remainingMl;
    }

    public void setRemainingMl(Float remainingMl) {
        this.remainingMl = remainingMl;
    }

    public Float getTotalMl() {
        return totalMl;
    }

    public void setTotalMl(Float totalMl) {
        this.totalMl = totalMl;
    }


    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getBottleDetail() {
        return bottleDetail;
    }

    public void setBottleDetail(String bottleDetail) {
        this.bottleDetail = bottleDetail;
    }

    public Integer getBottleMl() {
        return bottleMl;
    }

    public void setBottleMl(Integer bottleMl) {
        this.bottleMl = bottleMl;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public String getGiftDate() {
        return giftDate;
    }

    public void setGiftDate(String giftDate) {
        this.giftDate = giftDate;
    }

    public Boolean getAccepted() {
        return isAccepted;
    }

    public void setAccepted(Boolean accepted) {
        isAccepted = accepted;
    }

    public Integer getGiftedMl() {
        return giftedMl;
    }

    public void setGiftedMl(Integer giftedMl) {
        this.giftedMl = giftedMl;
    }
}
