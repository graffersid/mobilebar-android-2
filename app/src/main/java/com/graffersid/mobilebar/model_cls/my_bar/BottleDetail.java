package com.graffersid.mobilebar.model_cls.my_bar;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 24/09/2019 at 01:01 PM.
 */
public class BottleDetail {

    @SerializedName("bottle_id")
    @Expose
    private Integer bottleId;
    @SerializedName("customer_bottle_id")
    @Expose
    private Integer customerBottleId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("expiry")
    @Expose
    private Object expiry;
    @SerializedName("purchased_bottle")
    @Expose
    private Integer purchasedBottle;

    @SerializedName("remaining_ml")
    @Expose
    private Double remainingMl;
    @SerializedName("total_ml")
    @Expose
    private Double totalMl;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("category")
    @Expose
    private String category;

    public Integer getBottleId() {
        return bottleId;
    }

    public void setBottleId(Integer bottleId) {
        this.bottleId = bottleId;
    }

    public Integer getCustomerBottleId() {
        return customerBottleId;
    }

    public void setCustomerBottleId(Integer customerBottleId) {
        this.customerBottleId = customerBottleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Object getExpiry() {
        return expiry;
    }

    public void setExpiry(Object expiry) {
        this.expiry = expiry;
    }

    public Double getRemainingMl() {
        return remainingMl;
    }

    public void setRemainingMl(Double remainingMl) {
        this.remainingMl = remainingMl;
    }

    public Double getTotalMl() {
        return totalMl;
    }

    public void setTotalMl(Double totalMl) {
        this.totalMl = totalMl;
    }

    public Integer getPurchasedBottle() {
        return purchasedBottle;
    }

    public void setPurchasedBottle(Integer purchasedBottle) {
        this.purchasedBottle = purchasedBottle;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
